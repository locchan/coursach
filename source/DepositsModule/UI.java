/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DepositsModule;

import java.awt.Color;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JComboBox;
import javax.swing.JTextArea;
import org.json.simple.JSONObject;

/**
 *
 * @author Administrator
 */
public class UI extends javax.swing.JFrame {
    private static Date currentDate;
    private static ArrayList<String> clientsList = new ArrayList<>();
    private void initialize(){
        try {
            clientsList.clear();
            Client.removeAllItems();
            DepositName.removeAllItems();
            DepositTerm.removeAllItems();
            DepositCurrency.removeAllItems();
            DepositInterest.removeAllItems();
            DepositType.removeAllItems();
            DepositsNetworking.sendDataAndGetResponce(JsonHandling.createJSON(
                    "GETAVAILABLEDEPOSITS", null, null), null);
            DepositsNetworking.sendDataAndGetResponce(JsonHandling.createJSON(
                    "GETALLCLIENTSINFO", null, null), null);
            getCurrentBankDate();
            insertClients(Client);
            insertTerms(DepositTerm);
            insertCurrencies(DepositCurrency);
            insertInterests(DepositInterest);
            insertTypes(DepositType);
            insertDepositNamesByData();
                    } catch (IOException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void countRevenue(){
        double amount = Double.parseDouble(DepositAmount.getText());
        Available_Deposit dep = Available_Deposit.getAvailableDepositByNameAndCurrency(DepositName.getSelectedItem().toString(), DepositCurrency.getSelectedItem().toString());
        String term = dep.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.");
        if(term.contains("Unlimited")) return;
        double multiplier = 0;
        String[] termarr = term.split(" ");
        if(termarr[1].equals("г.")) multiplier = Double.parseDouble(termarr[0]);
        if(termarr[1].equals("мес.")) multiplier = Double.parseDouble(termarr[0])/12;
        if(termarr[1].equals("дн.")) multiplier = Double.parseDouble(termarr[0])/365;
        double interest = Double.parseDouble(dep.getInterest());
        double finalAmount = amount + amount*(interest/100)*multiplier;
        double finalProfit = amount*(interest/100)*multiplier;
        DepositRevenue.setText("Выручка по депозиту: "+finalAmount);
        DepositProfit.setText("Чистая прибыль по депозиту: "+finalProfit);
    }
    private void checkDepositAmount(){
        try{
        String input = DepositAmount.getText();
        double amount = Double.parseDouble(input);
        if(amount<Double.parseDouble(Available_Deposit.getAvailableDepositByNameAndCurrency(DepositName.getSelectedItem().toString(),DepositCurrency.getSelectedItem().toString()).getMin_deposit())){
            DepositAmount.setBackground(Color.red);
        } else{
            DepositAmount.setBackground(Color.white);
            countRevenue();
        }
        } catch(NumberFormatException e){
            DepositAmount.setBackground(Color.red);
        }
    }
    private void getCurrentBankDate(){
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("JSON","BELARUSBANK");
            jsonObj.put("TYPE","GETDATE");
            DepositsNetworking.sendDataAndGetResponce(jsonObj.toJSONString(), null);
        } catch (IOException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void setCurrentDate(Date date) {
        currentDate = date;
    }
    private String updateStartEndDate(){
        getCurrentBankDate();
        if(DepositName.getSelectedItem().toString().equals("")) return null;
         Available_Deposit currDep = Available_Deposit.getAvailableDepositByName(DepositName.getSelectedItem().toString());
         String text = "";
         SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
         Date dt = currentDate;
         Calendar c = Calendar.getInstance();
         String endDate = "";
         c.setTime(dt);
         text+=dateFormat.format(dt)+" - ";
         String[] splitTerm = currDep.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").replaceAll("Unlimited", "До востребования").split(" ");
         switch(splitTerm[1]){
             case "дн.":
                 c.add(Calendar.DAY_OF_YEAR,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate = dateFormat.format(c.getTime());
                 break;
             case "мес.":
                 c.add(Calendar.MONTH,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate =  dateFormat.format(c.getTime());
                 break; 
             case "г.":
                 c.add(Calendar.YEAR,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate = dateFormat.format(c.getTime());
                 break;
             default:
                 StartEndDates.setText(text+"до востребования");
                 endDate = "Unlimited";
         }
         StartEndDates.setText(text);
         return endDate;
     }
    private void writeDepositInfo(){
        System.out.println("WRITING");
        if(DepositName.getSelectedItem().toString().equals("")){return;}
         ArrayList<Available_Deposit> depList = Available_Deposit.getAvailableDepositsByName(DepositName.getSelectedItem().toString());
         if(depList.size()==1){
             Available_Deposit dep = depList.get(0);
             String text = "==========Депозит:========\n"+dep.getName()+"\nВалюта: "+dep.getCurrency()+"\nТип депозита: "+dep.getType()+
                     "\nСрок депозита: "+dep.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").replaceAll("Unlimited", "До востребования")+
                     "\nПроцент по депозиту: "+dep.getInterest()+"%\nМинимальная сумма депозита: "+dep.getMin_deposit()+"\n==========================\n";
             Console.append(text);
             System.out.println(text);
             return;
         }
         Available_Deposit dep = depList.get(0);
          String text = "==========Депозит:========\n"+dep.getName()+"\n=Возможные варианты валют=";
        for(int i=0;i<depList.size();i++){
            dep = depList.get(i);
            text+="\nВалюта: "+dep.getCurrency()+
                     "\nПроцент по депозиту: "+dep.getInterest()+"%\nМинимальная сумма депозита: "+dep.getMin_deposit()+"\n";
        }       
         text+="==========================\nТип депозита: "+dep.getType()+
                     "\nСрок депозита: "+dep.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").replaceAll("Unlimited", "До востребования")+"\n==========================\n";
         Console.append(text);
         System.out.println(text);
    }
    private void setCurrenciesByDeposit(){
         clearFields();
         ArrayList<Available_Deposit> depList = Available_Deposit.getAvailableDepositsByName(DepositName.getSelectedItem().toString());
         ArrayList<String> currenciesList = new ArrayList<>();
         for(int i=0;i<depList.size();i++){
             if(!currenciesList.contains(depList.get(i).getCurrency())){
                 currenciesList.add(depList.get(i).getCurrency());
             }
         }
         DepositCurrency.removeAllItems();
         for(int i=0;i<currenciesList.size();i++){
             DepositCurrency.addItem(currenciesList.get(i));
         }
    }
    private void clearFields(){
        System.out.println("CLEARING");
        selectJComboBoxElementByName("", DepositCurrency);
        selectJComboBoxElementByName("", DepositInterest);
        selectJComboBoxElementByName("", DepositTerm);
        selectJComboBoxElementByName("", DepositType);
        DepositAmount.setText("");
        Console.setText("");
    }
    public static synchronized ArrayList<String> getClientsList() {
        return clientsList;
    }
    private void insertDataByDepositName(){
        if(DepositName.getSelectedItem().toString().equals("")){ clearFields(); return;}
        String depositName = DepositName.getSelectedItem().toString();
        Available_Deposit deposit = Available_Deposit.getAvailableDepositByName(depositName);
        selectJComboBoxElementByName(deposit.getCurrency(), DepositCurrency);
        selectJComboBoxElementByName(deposit.getInterest(), DepositInterest);
        selectJComboBoxElementByName(deposit.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").replaceAll("Unlimited", "До востребования"), DepositTerm);
        selectJComboBoxElementByName(deposit.getType(), DepositType);
    }
    private void selectJComboBoxElementByName(String name, JComboBox<String> jcombobox){
        for(int i=0;i<jcombobox.getItemCount();i++){
            if(jcombobox.getItemAt(i).toString().equals(name)) jcombobox.setSelectedIndex(i);
    }
    }
    private void insertDepositNamesByData(){
        DepositName.removeAllItems();
        String DepCurr, DepType, DepTerm, DepInter, DepAm;
        if(DepositCurrency.getSelectedItem()!=null){ DepCurr = DepositCurrency.getSelectedItem().toString(); }
        else DepCurr = "";
        if(DepositType.getSelectedItem()!=null){ DepType = DepositType.getSelectedItem().toString(); }
        else DepType = "";
        if(DepositTerm.getSelectedItem()!=null){ DepTerm = DepositTerm.getSelectedItem().toString(); }
        else DepTerm = "";
        if(DepositInterest.getSelectedItem()!=null){ DepInter = DepositInterest.getSelectedItem().toString(); }
        else DepInter = "";
        if(DepositAmount.getText()!=null){ DepAm = DepositAmount.getText(); }
        else DepAm= "";
        ArrayList<String> matchingDeposits = Available_Deposit.getDepositByData(DepCurr,
                DepType, DepTerm, DepInter, DepAm);
        if(matchingDeposits.size()==0){
            DepositName.addItem("Нет депозитов на таких условиях");
        }else{
            DepositName.addItem("");
            for(int i=0;i<matchingDeposits.size();i++){
                DepositName.addItem(matchingDeposits.get(i));
            }
        }
    }
    private void insertTypes(JComboBox<String> typesBox){
        typesBox.addItem("");
        ArrayList<String> distinctTypesList = Available_Deposit.getDistinctTypes();
        for(int i=0;i<distinctTypesList.size();i++){
        typesBox.addItem(distinctTypesList.get(i));
        }
    }
    private void insertClients(JComboBox<String> clientsBox){
        clientsBox.addItem("");
        for(int i=0;i<getClientsList().size();i++){
        clientsBox.addItem(getClientsList().get(i));
        }
    }
    private void insertTerms(JComboBox<String> termsBox){
        termsBox.addItem("");
        ArrayList<String> distinctTermsList = Available_Deposit.getDistinctTerm();
        for(int i=0;i<distinctTermsList.size();i++){
        termsBox.addItem(distinctTermsList.get(i));
        }
    }
    private void insertCurrencies(JComboBox<String> currenciesBox){
        currenciesBox.addItem("");
        ArrayList<String> distinctCurrenciesList = Available_Deposit.getDistinctCurrencies();
        for(int i=0;i<distinctCurrenciesList.size();i++){
        currenciesBox.addItem(distinctCurrenciesList.get(i));
        }
    }
    private void insertInterests(JComboBox<String> interestsBox){
        interestsBox.addItem("");
        ArrayList<String> distinctCurrenciesList = Available_Deposit.getDistinctInterests();
        for(int i=0;i<distinctCurrenciesList.size();i++){
        interestsBox.addItem(distinctCurrenciesList.get(i));
        }
    }
    /**
     * Creates new form UI
     */
    public UI() {
        initComponents();
    }
    public JComboBox<String> getClient() {
        return Client;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Client = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        DepositAmount = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        DepositTerm = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        DepositName = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        DepositCurrency = new javax.swing.JComboBox<>();
        DepositProfit = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        DepositType = new javax.swing.JComboBox<>();
        DepositRevenue = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        DepositInterest = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        Console = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        JButton1 = new javax.swing.JButton();
        SelectionMode = new javax.swing.JCheckBox();
        StartEndDates = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Модуль \"Депозиты\"");

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setText("Депозиты");

        Client.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClientActionPerformed(evt);
            }
        });

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Клиент");

        DepositAmount.setEnabled(false);
        DepositAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositAmountActionPerformed(evt);
            }
        });
        DepositAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                DepositAmountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                DepositAmountKeyTyped(evt);
            }
        });

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Сумма депозита");

        DepositTerm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositTermActionPerformed(evt);
            }
        });

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Срок депозита");

        DepositName.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                DepositNamePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        DepositName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                DepositNameMouseClicked(evt);
            }
        });
        DepositName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositNameActionPerformed(evt);
            }
        });

        jLabel6.setText("Депозит");

        jLabel7.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel7.setText("Валюта депозита");

        DepositCurrency.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                DepositCurrencyPopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        DepositCurrency.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositCurrencyActionPerformed(evt);
            }
        });

        DepositProfit.setText("Чистый доход по депозиту:");

        jLabel9.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel9.setText("Тип депозита");

        DepositType.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositTypeActionPerformed(evt);
            }
        });

        DepositRevenue.setText("Выручка по депозиту:");

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Процент по депозиту");

        DepositInterest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                DepositInterestActionPerformed(evt);
            }
        });

        Console.setEditable(false);
        Console.setColumns(20);
        Console.setLineWrap(true);
        Console.setRows(5);
        jScrollPane1.setViewportView(Console);

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Консоль");

        JButton1.setText("Заключить договор");
        JButton1.setEnabled(false);
        JButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JButton1MouseClicked(evt);
            }
        });

        SelectionMode.setText("Оформление договора");
        SelectionMode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                SelectionModeActionPerformed(evt);
            }
        });

        StartEndDates.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        StartEndDates.setText(" ");

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Дата начала и конца депозитной программы");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(10, 10, 10)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel2, javax.swing.GroupLayout.DEFAULT_SIZE, 348, Short.MAX_VALUE)
                            .addComponent(Client, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(SelectionMode, javax.swing.GroupLayout.PREFERRED_SIZE, 260, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addGap(0, 0, Short.MAX_VALUE)
                                .addComponent(JButton1))
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(StartEndDates, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(jLabel6)
                                        .addComponent(DepositName, javax.swing.GroupLayout.PREFERRED_SIZE, 348, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createSequentialGroup()
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                                .addComponent(jLabel4, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel3, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(DepositTerm, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(DepositAmount, javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(DepositCurrency, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                            .addGap(18, 18, 18)
                                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(DepositInterest, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(jLabel5, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(DepositRevenue, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addComponent(DepositProfit, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, 130, Short.MAX_VALUE)
                                        .addComponent(DepositType, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 325, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(Client, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(SelectionMode)))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(jLabel8))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(DepositName, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(jLabel5))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DepositTerm, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(DepositInterest, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DepositAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(DepositRevenue))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(DepositCurrency, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(DepositProfit))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(DepositType, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 60, Short.MAX_VALUE)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(StartEndDates, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(JButton1))
                    .addComponent(jScrollPane1))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClientActionPerformed
        
    }//GEN-LAST:event_ClientActionPerformed

    private void DepositAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositAmountActionPerformed
        if(!SelectionMode.isSelected()){
        insertDepositNamesByData();
        }
    }//GEN-LAST:event_DepositAmountActionPerformed

    private void DepositNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositNameActionPerformed
        
    }//GEN-LAST:event_DepositNameActionPerformed

    private void DepositTypeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositTypeActionPerformed
        if(!SelectionMode.isSelected()){
            insertDepositNamesByData();
        }
    }//GEN-LAST:event_DepositTypeActionPerformed

    private void DepositTermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositTermActionPerformed
        if(!SelectionMode.isSelected()){
        insertDepositNamesByData();
        }
        if(!DepositName.getSelectedItem().toString().equals("Нет депозитов на таких условиях")){
        updateStartEndDate();
        }
    }//GEN-LAST:event_DepositTermActionPerformed

    public JTextArea getConsole() {
        return Console;
    }

    private void DepositCurrencyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositCurrencyActionPerformed
       if(!SelectionMode.isSelected()){
        insertDepositNamesByData();
       }
    }//GEN-LAST:event_DepositCurrencyActionPerformed

    private void DepositInterestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_DepositInterestActionPerformed
        if(!SelectionMode.isSelected()){
        insertDepositNamesByData();
        }
    }//GEN-LAST:event_DepositInterestActionPerformed
    private static String lastselected = "";
    private void DepositNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_DepositNameMouseClicked
      
    }//GEN-LAST:event_DepositNameMouseClicked

    private void SelectionModeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_SelectionModeActionPerformed
        if(SelectionMode.isSelected()){
            JButton1.setEnabled(true);
            DepositAmount.setEnabled(true);
        } else {
            JButton1.setEnabled(false);
            DepositAmount.setEnabled(false);
            
        }
        String beforeClear = DepositName.getSelectedItem().toString();
        clearFields();
        insertDepositNamesByData();
        selectJComboBoxElementByName(beforeClear, DepositName);
        insertDataByDepositName();
        if(SelectionMode.isSelected()){
        writeDepositInfo();
        DepositType.setEnabled(false);
        DepositTerm.setEnabled(false);
        DepositInterest.setEnabled(false);
        } else {
        DepositCurrency.removeAllItems();
        insertCurrencies(DepositCurrency);
        DepositType.setEnabled(true);
        DepositTerm.setEnabled(true);
        DepositInterest.setEnabled(true);
        }
    }//GEN-LAST:event_SelectionModeActionPerformed

    private void DepositNamePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_DepositNamePopupMenuWillBecomeInvisible
       clearFields();
       writeDepositInfo();
        if(!lastselected.equals(DepositName.getSelectedItem().toString())){
        if(SelectionMode.isSelected()){
        clearFields();
        setCurrenciesByDeposit();
        insertDataByDepositName();
        writeDepositInfo();

        lastselected = DepositName.getSelectedItem().toString();
      }
      }
    }//GEN-LAST:event_DepositNamePopupMenuWillBecomeInvisible

    private void DepositAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DepositAmountKeyTyped
      
    }//GEN-LAST:event_DepositAmountKeyTyped

    private void DepositAmountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_DepositAmountKeyReleased
        checkDepositAmount();
    }//GEN-LAST:event_DepositAmountKeyReleased
    
    private void DepositCurrencyPopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_DepositCurrencyPopupMenuWillBecomeInvisible
        if(!DepositInterest.isEnabled()){
            Available_Deposit dep = Available_Deposit.getAvailableDepositByNameAndCurrency(DepositName.getSelectedItem().toString(), DepositCurrency.getSelectedItem().toString());
            selectJComboBoxElementByName(dep.getInterest(), DepositInterest);
            checkDepositAmount();  
        }
    }//GEN-LAST:event_DepositCurrencyPopupMenuWillBecomeInvisible

    private void JButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JButton1MouseClicked
       if(DepositAmount.getBackground().equals(Color.WHITE)){
           try {
               long endtime;
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
               if(!updateStartEndDate().equals("Unlimited")){
                 endtime = sdf.parse(updateStartEndDate()).getTime();
               } else{
                 endtime = 0L;
               }
               accounting.signContract(Client.getSelectedItem().toString(), DepositName.getSelectedItem().toString(), DepositAmount.getText(),DepositCurrency.getSelectedItem().toString(), String.valueOf(currentDate),String.valueOf(endtime), this);
           } catch (ParseException ex) {
               Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
           }
       }
    }//GEN-LAST:event_JButton1MouseClicked
    public static void createNew(){
        main(null);
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                UI gui = new UI();
                gui.initialize();
                gui.setVisible(true);

            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Client;
    private javax.swing.JTextArea Console;
    private javax.swing.JTextField DepositAmount;
    private javax.swing.JComboBox<String> DepositCurrency;
    private javax.swing.JComboBox<String> DepositInterest;
    private javax.swing.JComboBox<String> DepositName;
    private javax.swing.JLabel DepositProfit;
    private javax.swing.JLabel DepositRevenue;
    private javax.swing.JComboBox<String> DepositTerm;
    private javax.swing.JComboBox<String> DepositType;
    private javax.swing.JButton JButton1;
    private javax.swing.JCheckBox SelectionMode;
    private javax.swing.JLabel StartEndDates;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
