/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DepositsModule;

import java.util.ArrayList;
import java.util.Collections;

/**
 *
 * @author Administrator
 */
public class Available_Deposit {
    private static final ArrayList<Available_Deposit> available_deposits = new ArrayList<>();
    private final int id;
    private final String name;
    private final String currency;
    private final String type;
    private final String term;
    private final String min_deposit;
    private final String interest;

    public Available_Deposit(String name, String currency, String type, String term, String min_deposit, String interest, int id) {
        this.name = name;
        this.currency = currency;
        this.type = type;
        this.term = term;
        this.min_deposit = min_deposit;
        this.interest = interest;
        this.id = id;
        available_deposits.add(this);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCurrency() {
        return currency;
    }

    public String getType() {
        return type;
    }

    public String getTerm() {
        return term;
    }

    public String getMin_deposit() {
        return min_deposit;
    }

    public String getInterest() {
        return interest;
    }
    public static Available_Deposit getAvailableDepositByName(String name){
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getName().equals(name)) return available_deposits.get(i);
        }
        return null;
    }
    public static Available_Deposit getAvailableDepositByNameAndCurrency(String name, String currency){
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getName().equals(name)&&available_deposits.get(i).getCurrency().equals(currency)) return available_deposits.get(i);
        }
        return null;
    }
    public static ArrayList<Available_Deposit> getAvailableDepositsByName(String name){
        ArrayList<Available_Deposit> deposits = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getName().equals(name)) deposits.add(available_deposits.get(i));
        }
        return deposits;
    }
    public static ArrayList<String> getDistinctTerm(){
        ArrayList<String> distinctTerms = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getTerm().contains("D")&&!distinctTerms.contains(available_deposits.get(i).getTerm().replaceAll("D", " дн."))){
                distinctTerms.add(available_deposits.get(i).getTerm().replaceAll("D", " дн."));
            }
        }
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getTerm().contains("M")&&!distinctTerms.contains(available_deposits.get(i).getTerm().replaceAll("M", " мес."))){
                distinctTerms.add(available_deposits.get(i).getTerm().replaceAll("M", " мес."));
            }
        }
        for(int i=0;i<available_deposits.size();i++){
            if(available_deposits.get(i).getTerm().contains("Y")&&!distinctTerms.contains(available_deposits.get(i).getTerm().replaceAll("Y", " г."))){
                distinctTerms.add(available_deposits.get(i).getTerm().replaceAll("Y", " г."));
            }
        }
        return distinctTerms;
    }
    public static ArrayList<String> getDistinctCurrencies(){
        ArrayList<String> distinctCurrencies = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
            if(!distinctCurrencies.contains(available_deposits.get(i).getCurrency())){
                distinctCurrencies.add(available_deposits.get(i).getCurrency());
            }
        }
        return distinctCurrencies;
    }
    public static ArrayList<String> getDistinctTypes(){
        ArrayList<String> distinctTypes = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
            if(!distinctTypes.contains(available_deposits.get(i).getType())){
                distinctTypes.add(available_deposits.get(i).getType());
            }
        }
        return distinctTypes;
    }
    public static ArrayList<String> getDistinctInterests(){
        ArrayList<String> distinctInterests = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
            if(!distinctInterests.contains(available_deposits.get(i).getInterest())){
                distinctInterests.add(available_deposits.get(i).getInterest());
            }
        }
        Collections.sort(distinctInterests);
        Collections.reverse(distinctInterests);
        return distinctInterests;
    }
    public static ArrayList<String> getDepositByData(String currency, String type, String term, String interest, String deposit_amount){
        try{
        ArrayList<Available_Deposit> matchingDeposits = new ArrayList<>();
        for(int i=0;i<available_deposits.size();i++){
        Available_Deposit depToTest = available_deposits.get(i);
        if(!currency.equals("")){
                if(!depToTest.getCurrency().equals(currency)){
                    continue;
            }
        }
        if(!type.equals("")){
                if(!depToTest.getType().equals(type)){
                    continue;
            }
        }
        if(!term.equals("")){
                if(!depToTest.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").replaceAll("Unlimited", "До востребования").equals(term)){
                    continue;
            }
        }
        if(!interest.equals("")){
                if(!depToTest.getInterest().equals(interest)){
                    continue;
                }
        }
        if(!deposit_amount.equals("")){
        int dep_am = Integer.parseInt(deposit_amount);
        int dep_min_am = Integer.parseInt(depToTest.getMin_deposit());
        if(dep_min_am>dep_am){
        continue;
        }
        }
        matchingDeposits.add(depToTest);
        }
        ArrayList<String> matchingDepositNames = new ArrayList<>();
        for(int i=0;i<matchingDeposits.size();i++){
            if(!matchingDepositNames.contains(matchingDeposits.get(i).getName()))
            matchingDepositNames.add(matchingDeposits.get(i).getName());
        }

        return matchingDepositNames;
        }catch (NumberFormatException e){
            System.out.println("Неверная сумма вклада!");
        }
        return null;
    }
}
