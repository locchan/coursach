/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CreditsModule;

import java.io.IOException;
import java.text.ParseException;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Administrator
 */
public class accounting {
    public static void signContract(String client, String creditName, String creditAmount, String startDate, String endDate, UI gui, String creditTerm){
        String[] clientArr = client.split(", ");
        String clientID = clientArr[1].replaceAll("\\)","");
        String currentAccountNumber;
        String interestAccountNumber;
        String handledEndDate;
        if(endDate.equals(0L)){
            handledEndDate = null;
        } else handledEndDate = endDate;
        interestAccountNumber="2470"+generateRandom(7)+"1";
        currentAccountNumber = "2400"+generateRandom(7)+"1";
        try {
            CreditsNetworking.sendDataAndGetResponce(JsonHandling.createContractSigningJSON(clientID, creditName, creditAmount, startDate, handledEndDate, currentAccountNumber, interestAccountNumber, creditTerm), gui);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(accounting.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static long generateRandom(int length) {
    Random random = new Random();
    char[] digits = new char[length];
    digits[0] = (char) (random.nextInt(9) + '1');
    for (int i = 1; i < length; i++) {
        digits[i] = (char) (random.nextInt(10) + '0');
    }
    return Long.parseLong(new String(digits));
}
}
