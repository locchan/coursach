/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CreditsModule;

import java.awt.Color;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.sql.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import org.json.simple.JSONObject;

/**
 *
 * @author Administrator
 */
public class UI extends javax.swing.JFrame {
    private static Date currentDate;
    private static final ArrayList<String> clientsList = new ArrayList<>();
    private void initialize(){
        try {
            clientsList.clear();
            Client.removeAllItems();
            CreditName.removeAllItems();
            CreditsNetworking.sendDataAndGetResponce(JsonHandling.createJSON(
                    "GETAVAILABLECREDITS", null, null), null);
            CreditsNetworking.sendDataAndGetResponce(JsonHandling.createJSON(
                    "GETALLCLIENTSINFO", null, null), null);
            getCurrentBankDate();
            insertClients(Client);
            insertCredits(CreditName);
                    } catch (IOException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ParseException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    private void getCurrentBankDate(){
        try {
            JSONObject jsonObj = new JSONObject();
            jsonObj.put("JSON","BELARUSBANK");
            jsonObj.put("TYPE","GETDATE");
            CreditsNetworking.sendDataAndGetResponce(jsonObj.toJSONString(), null);
        } catch (IOException | ParseException ex) {
            Logger.getLogger(UI.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public static void setCurrentDate(Date date) {
        currentDate = date;
    }
    private String updateStartEndDate(){
        getCurrentBankDate();
        if(CreditName.getSelectedItem().toString().equals("")) return null;
        String[] creditNameArr =  CreditName.getSelectedItem().toString().split("\\(");
        String creditName = creditNameArr[0].trim();
        String creditTerm =  creditNameArr[1].replaceAll("\\)","").trim();
         Available_Credit currCred = Available_Credit.getAvailableCreditByNameAndTerm(creditName, creditTerm.replaceAll(" дн.","D").replaceAll(" мес.","M").replaceAll(" г.", "Y"));
         String text = "";
         SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
         Date dt = currentDate;
         Calendar c = Calendar.getInstance();
         String endDate = "";
         c.setTime(dt);
         text+=dateFormat.format(dt)+" - ";
         String[] splitTerm = currCred.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.").split(" ");
         switch(splitTerm[1]){
             case "дн.":
                 c.add(Calendar.DAY_OF_YEAR,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate = dateFormat.format(c.getTime());
                 break;
             case "мес.":
                 c.add(Calendar.MONTH,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate =  dateFormat.format(c.getTime());
                 break; 
             case "г.":
                 c.add(Calendar.YEAR,Integer.parseInt(splitTerm[0]));
                 dateFormat.setTimeZone(c.getTimeZone());
                 text+=dateFormat.format(c.getTime());
                 endDate = dateFormat.format(c.getTime());
                 break;
         }
         StartEndDates.setText(text);
         return endDate;
     }
    private void writeCreditInfo(){
       
    }
    private void clearFields(){
        System.out.println("CLEARING");
        selectJComboBoxElementByName("", CreditTerm);
        CreditAmount.setText("");
        Console.setText("");
    }
    public static synchronized ArrayList<String> getClientsList() {
        return clientsList;
    }
    private void insertDataByCreditName(){
        if(CreditName.getSelectedItem().toString().equals("")){ clearFields(); return;}
        String[] creditNameArr =  CreditName.getSelectedItem().toString().split("\\(");
        String creditName = creditNameArr[0].trim();
        String creditTerm =  creditNameArr[1].replaceAll("\\)","").trim();
        Available_Credit credit = Available_Credit.getAvailableCreditByNameAndTerm(creditName, creditTerm.replaceAll(" дн.","D").replaceAll(" мес.","M").replaceAll(" г.", "Y"));
        CreditTerm.removeAllItems();
        CreditTerm.addItem(credit.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г."));
        selectJComboBoxElementByName(credit.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г."), CreditTerm);
        CreditInterest.addItem(credit.getInterest());
        selectJComboBoxElementByName(credit.getInterest(), CreditInterest);
    }
    private void selectJComboBoxElementByName(String name, JComboBox<String> jcombobox){
        for(int i=0;i<jcombobox.getItemCount();i++){
            if(jcombobox.getItemAt(i).equals(name)) jcombobox.setSelectedIndex(i);
    }
    }
    private void insertCredits(JComboBox<String> creditsBox){
        creditsBox.addItem("");
        for(int i=0;i<Available_Credit.getAvailable_credits().size();i++){
            Available_Credit credit = Available_Credit.getAvailable_credits().get(i);
        creditsBox.addItem(credit.getName()+"("+credit.getTerm().replaceAll("D", " дн.").replaceAll("M", " мес.").replaceAll("Y", " г.")+")");
        }
    }
    private void insertClients(JComboBox<String> clientsBox){
        clientsBox.addItem("");
        for(int i=0;i<getClientsList().size();i++){
        clientsBox.addItem(getClientsList().get(i));
        }
    }
   /**
     * Creates new form UI
     */
    public UI() {
        initComponents();
    }
    public JComboBox<String> getClient() {
        return Client;
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        Client = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        CreditAmount = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        CreditTerm = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        CreditName = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        CreditInterestSum = new javax.swing.JLabel();
        CreditDebt = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        CreditInterest = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        Console = new javax.swing.JTextArea();
        jLabel8 = new javax.swing.JLabel();
        JButton1 = new javax.swing.JButton();
        StartEndDates = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        CreditDebt1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Модуль \"Кредиты\"");
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Dialog", 1, 24)); // NOI18N
        jLabel1.setText("Кредиты");
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(12, 12, -1, 51));

        Client.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ClientActionPerformed(evt);
            }
        });
        getContentPane().add(Client, new org.netbeans.lib.awtextra.AbsoluteConstraints(141, 21, 460, -1));

        jLabel2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel2.setText("Клиент");
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(141, 0, 460, -1));

        CreditAmount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreditAmountActionPerformed(evt);
            }
        });
        CreditAmount.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                CreditAmountKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                CreditAmountKeyTyped(evt);
            }
        });
        getContentPane().add(CreditAmount, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 192, 130, -1));

        jLabel3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel3.setText("Сумма кредита");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 171, 130, -1));

        CreditTerm.setEnabled(false);
        CreditTerm.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreditTermActionPerformed(evt);
            }
        });
        getContentPane().add(CreditTerm, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 141, 130, -1));

        jLabel4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel4.setText("Срок кредита");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 120, 130, -1));

        CreditName.addPopupMenuListener(new javax.swing.event.PopupMenuListener() {
            public void popupMenuCanceled(javax.swing.event.PopupMenuEvent evt) {
            }
            public void popupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {
                CreditNamePopupMenuWillBecomeInvisible(evt);
            }
            public void popupMenuWillBecomeVisible(javax.swing.event.PopupMenuEvent evt) {
            }
        });
        CreditName.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                CreditNameMouseClicked(evt);
            }
        });
        CreditName.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreditNameActionPerformed(evt);
            }
        });
        getContentPane().add(CreditName, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 90, 270, -1));

        jLabel6.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel6.setText("Кредит");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 70, 270, -1));

        CreditInterestSum.setText("Ежемесячная выплата по процентам:");
        getContentPane().add(CreditInterestSum, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 320, -1));

        CreditDebt.setText("Ежемесячная выплата по кредиту:");
        getContentPane().add(CreditDebt, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 260, 320, -1));

        jLabel5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel5.setText("Процент по кредиту");
        getContentPane().add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 120, 130, -1));

        CreditInterest.setEnabled(false);
        CreditInterest.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                CreditInterestActionPerformed(evt);
            }
        });
        getContentPane().add(CreditInterest, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 140, 130, -1));

        Console.setEditable(false);
        Console.setColumns(20);
        Console.setLineWrap(true);
        Console.setRows(5);
        jScrollPane1.setViewportView(Console);

        getContentPane().add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 70, 280, 260));

        jLabel8.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel8.setText("Консоль");
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, 280, -1));

        JButton1.setText("Заключить договор");
        JButton1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                JButton1MouseClicked(evt);
            }
        });
        JButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JButton1ActionPerformed(evt);
            }
        });
        getContentPane().add(JButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 340, -1, -1));

        StartEndDates.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        StartEndDates.setText(" ");
        getContentPane().add(StartEndDates, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 320, 300, 22));

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel10.setText("Дата начала и конца кредитной программы");
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, 290, 20));

        CreditDebt1.setText("Ежемесячная выплата по основному долгу:");
        getContentPane().add(CreditDebt1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 240, 320, -1));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ClientActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ClientActionPerformed
        
    }//GEN-LAST:event_ClientActionPerformed

    private void CreditAmountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreditAmountActionPerformed

    }//GEN-LAST:event_CreditAmountActionPerformed

    private void CreditNameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreditNameActionPerformed
        
    }//GEN-LAST:event_CreditNameActionPerformed

    private void CreditTermActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreditTermActionPerformed

    }//GEN-LAST:event_CreditTermActionPerformed

    public JTextArea getConsole() {
        return Console;
    }

    private void CreditInterestActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_CreditInterestActionPerformed

    }//GEN-LAST:event_CreditInterestActionPerformed

    private void CreditNameMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_CreditNameMouseClicked
      
    }//GEN-LAST:event_CreditNameMouseClicked

    private void CreditNamePopupMenuWillBecomeInvisible(javax.swing.event.PopupMenuEvent evt) {//GEN-FIRST:event_CreditNamePopupMenuWillBecomeInvisible
       clearFields();
       writeCreditInfo();
        clearFields();
        insertDataByCreditName();
        writeCreditInfo();
    }//GEN-LAST:event_CreditNamePopupMenuWillBecomeInvisible

    public JTextField getCreditAmount() {
        return CreditAmount;
    }

    public JLabel getCreditDebt() {
        return CreditDebt;
    }

    public JComboBox<String> getCreditInterest() {
        return CreditInterest;
    }

    public JLabel getCreditInterestSum() {
        return CreditInterestSum;
    }

    public JButton getJButton1() {
        return JButton1;
    }

    public JComboBox<String> getCreditName() {
        return CreditName;
    }

    public JComboBox<String> getCreditTerm() {
        return CreditTerm;
    }

    private void CreditAmountKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CreditAmountKeyTyped
       
    }//GEN-LAST:event_CreditAmountKeyTyped

    public JLabel getCreditDebt1() {
        return CreditDebt1;
    }

    private void CreditAmountKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_CreditAmountKeyReleased
double creditSum = 0;
        try{
        creditSum = Double.parseDouble(getCreditAmount().getText());
       } catch (Exception e){
           getCreditAmount().setBackground(Color.red);
           getJButton1().setEnabled(false);
       } if(creditSum!=0){
           getCreditAmount().setBackground(Color.white);
           getJButton1().setEnabled(true);
       }
       String[] splitCreditName = getCreditName().getSelectedItem().toString().split("\\(");
       String creditTerm = splitCreditName[1].replaceAll("г.\\)", "").trim();
       int term = Integer.parseInt(creditTerm);
       double interest = Double.parseDouble(getCreditInterest().getSelectedItem().toString());
       getCreditInterestSum().setText("Ежемесячная выплата по процентам: " + String.format("%.2f",(creditSum*(interest/100))/12));
       getCreditDebt1().setText("Ежемесячная выплата по основному долгу: " + String.format("%.2f",(creditSum)/(12*term)));
       getCreditDebt().setText("Ежемесячная выплата по кредиту: "+String.format("%.2f", (((creditSum*(interest/100))/12))+(creditSum)/(12*term)));
       System.out.println(creditSum+";"+interest);
        
        
        
    }//GEN-LAST:event_CreditAmountKeyReleased
    
    private void JButton1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_JButton1MouseClicked

           try {
               long endtime;
               SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                 endtime = sdf.parse(updateStartEndDate()).getTime();
                 String[] creditNameArr =  CreditName.getSelectedItem().toString().split("\\(");
                  String creditName = creditNameArr[0].trim();
                    String creditTerm =  creditNameArr[1].replaceAll("\\)","").trim();
                  if(Double.parseDouble(CreditAmount.getText())<=0){
                      Console.append("Введите корректную сумму кредита\n");
                      return;
                  }
               accounting.signContract(Client.getSelectedItem().toString(), creditName, CreditAmount.getText(), String.valueOf(currentDate),String.valueOf(endtime), this, creditTerm.replaceAll(" дн.","D").replaceAll(" мес.","M").replaceAll(" г.", "Y"));
           } catch (ParseException | NumberFormatException | NullPointerException ex) {
               Console.append("Некорректный ввод\n");
           }
    }//GEN-LAST:event_JButton1MouseClicked

    private void JButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JButton1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_JButton1ActionPerformed
    public static void createNew(){
        main(null);
    }
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(UI.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            UI gui = new UI();
            gui.initialize();
            gui.setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Client;
    private javax.swing.JTextArea Console;
    private javax.swing.JTextField CreditAmount;
    private javax.swing.JLabel CreditDebt;
    private javax.swing.JLabel CreditDebt1;
    private javax.swing.JComboBox<String> CreditInterest;
    private javax.swing.JLabel CreditInterestSum;
    private javax.swing.JComboBox<String> CreditName;
    private javax.swing.JComboBox<String> CreditTerm;
    private javax.swing.JButton JButton1;
    private javax.swing.JLabel StartEndDates;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JScrollPane jScrollPane1;
    // End of variables declaration//GEN-END:variables
}
