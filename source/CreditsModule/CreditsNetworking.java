/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CreditsModule;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.ParseException;

/**
 *
 * @author Administrator
 */
public class CreditsNetworking {
       private static Socket socket = new Socket();
   private static final Thread t = new Thread();
        public static void sendDataAndGetResponce(String data, UI gui) throws IOException, ParseException{
           socket = new Socket("localhost", 37529);
            if(socket.isConnected()){
                BufferedWriter outpWriter = new BufferedWriter
                (new OutputStreamWriter(socket.getOutputStream()));
                BufferedReader inpReader = new BufferedReader
                (new InputStreamReader(socket.getInputStream()));
                outpWriter.write(data+"\n");
                System.out.println("SENDING: "+data+"\n");
                outpWriter.flush();
                String inputData = inpReader.readLine();
                System.out.println("RECIEVED: "+inputData+"\n");
                JsonHandling.parseJSON(inputData, gui);
                inpReader.close();
                outpWriter.close();
            } else System.err.println("CANNOT SEND DATA: SOCKET IS CLOSED");
        }
}
