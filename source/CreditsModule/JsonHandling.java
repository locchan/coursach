/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CreditsModule;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Administrator
 */
public class JsonHandling {
     public static String createJSON(String jsontype, String info1, String info2){
        JSONObject jsonObj = new JSONObject();
        switch(jsontype){
            case "GETLIST":
            jsonObj.put("JSON","BELARUSBANK");
            jsonObj.put("TYPE", jsontype);
            jsonObj.put("MODULE", "CREDITS");
            jsonObj.put("TABLE", info1);
            jsonObj.put("COLUMN", info2);
            return jsonObj.toJSONString();
            case "GETALLCLIENTSINFO":
            case "GETAVAILABLECREDITS":
            jsonObj.put("JSON","BELARUSBANK");
            jsonObj.put("MODULE", "CREDITS");
            jsonObj.put("TYPE", jsontype);
            return jsonObj.toJSONString();
        }
        return null;
    }
     public static void parseJSON(String jsonString, UI gui) throws java.text.ParseException{
         try {
             JSONParser parser = new JSONParser();
             JSONObject jsonObj = (JSONObject) parser.parse(jsonString);
             switch(jsonObj.get("TYPE").toString()){
             case("GETAVAILABLECREDITS"):
                 for(int i=0;;i++){
                    if(jsonObj.get("AVAILABLECREDIT"+i)==null) return;
                    String depStr = jsonObj.get("AVAILABLECREDIT"+i).toString();
                    JSONObject credit = (JSONObject) parser.parse(depStr);
                    new Available_Credit(credit.get("NAME").toString(),
                            credit.get("TERM").toString(),
                            credit.get("INTEREST").toString(),
                            Integer.parseInt(credit.get("ID").toString()));
                 }
              case("GETALLCLIENTSINFO"):
                  for(int i=0;;i++){
                    if(jsonObj.get("CLIENT"+i)==null) return;
                    String cliStr = jsonObj.get("CLIENT"+i).toString();
                    JSONObject client = (JSONObject) parser.parse(cliStr);
                    String clientStr = client.get("LASTNAME").toString()+" "+
                    client.get("FIRSTNAME").toString()+" "+
                    client.get("PATRONYM").toString()+" ("+
                    client.get("IDENTIFICNUMBER").toString()+", "+
                    client.get("ID")+")";
                    UI.getClientsList().add(clientStr);
                 }
                  case("INFO"):
                      System.out.println(jsonObj.get("DATA").toString());
                      gui.getConsole().append("Данные от сервера: "+jsonObj.get("DATA").toString()+"\n");
                      break;
                  case ("CONTRACTSIGNED"):
                      System.out.println("Contract was signed successfully. Contract ID: "+jsonObj.get("CONTRACTID").toString());
                      gui.getConsole().append("Договор успешно подписан. ID договора: "+jsonObj.get("CONTRACTID").toString()+"\n");
                      JSONObject accCreationJSON = new JSONObject();
                      accCreationJSON.put("JSON", "BELARUSBANK");
                      accCreationJSON.put("TYPE", "CREATECREDITACCOUNTS");
                      accCreationJSON.put("CONTRACTID",jsonObj.get("CONTRACTID"));
             {
                 try {
                     CreditsNetworking.sendDataAndGetResponce(accCreationJSON.toJSONString(), gui);
                 } catch (IOException | java.text.ParseException ex) {
                     Logger.getLogger(JsonHandling.class.getName()).log(Level.SEVERE, null, ex);
                 }
             }
                      break;
                  case ("GETDATE"):
                      SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                      UI.setCurrentDate(new java.sql.Date((sdf.parse(jsonObj.get("DATE").toString().replaceAll("\"","")).getTime())));
                      System.out.println("Date set.");
             }
         } catch (ParseException ex) {
             Logger.getLogger(JsonHandling.class.getName()).log(Level.SEVERE, null, ex);
         }
     }
     public static String createEntryMovingJSON(String accountFrom, String accountTo, String accountFromColumn, String accountToColumn, char dbt_crdt){
         JSONObject jsonObj = new JSONObject();
         jsonObj.put("JSON","BELARUSBANK");
         jsonObj.put("MODULE", "CREDITS");
         jsonObj.put("TYPE", "MOVEENTRIES");
         jsonObj.put("ACCOUNTFROM", accountFrom);
         jsonObj.put("ACCOUNTFROMCOLUMN",accountFromColumn);
         jsonObj.put("ACCOUNTTO", accountTo);
         jsonObj.put("ACCOUNTTOCOLUMN",accountToColumn);
         return jsonObj.toJSONString();
     }
     public static String createContractSigningJSON(String client, String creditType, String creditAmount,
          String startDate, String endDate, String currentAccount, String interestAccount, String creditTerm){
         JSONObject jsonObj = new JSONObject();
         jsonObj.put("JSON","BELARUSBANK");
         jsonObj.put("MODULE", "CREDITS");
         jsonObj.put("TYPE", "SIGNCREDITCONTRACT");
         jsonObj.put("CLIENTID", client);
         jsonObj.put("ID", Available_Credit.getAvailableCreditByNameAndTerm(creditType, creditTerm).getId());
         jsonObj.put("CREDITAMOUNT", creditAmount);
         jsonObj.put("STARTDATE", startDate);
         jsonObj.put("ENDDATE",endDate);
         jsonObj.put("CURRENTACCOUNT", currentAccount);
         jsonObj.put("INTERESTACCOUNT", interestAccount);
         return jsonObj.toJSONString();
     }
}
