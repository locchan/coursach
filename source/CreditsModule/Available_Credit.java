/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package CreditsModule;

import java.util.ArrayList;

/**
 *
 * @author Administrator
 */
public class Available_Credit {
    private static final ArrayList<Available_Credit> available_credits = new ArrayList<>();

    public static ArrayList<Available_Credit> getAvailable_credits() {
        return available_credits;
    }
    private final int id;
    private final String name;
    private final String term;
    private final String interest;

    public Available_Credit(String name, String term, String interest, int id) {
        this.name = name;
        this.term = term;
        this.interest = interest;
        this.id = id;
        available_credits.add(this);
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }
    public String getTerm() {
        return term;
    }

    public String getInterest() {
        return interest;
    }
    public static Available_Credit getAvailableCreditByNameAndTerm(String name, String term){
        for(int i=0;i<available_credits.size();i++){
            if(available_credits.get(i).getName().equals(name)){
                if(available_credits.get(i).getTerm().equals(term))
                return available_credits.get(i);
            }
        }
        return null;
    }
}
