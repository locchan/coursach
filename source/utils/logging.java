/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import server.bank;

/**
 *
 * @author Administrator
 */
public class logging {
    public static void log(String text){
        try {
            File logdir = new File("logs");
            if(!(logdir.exists())){
            logdir.mkdir();
            }
            File logfile = new File("logs/"+bank.getStartDateTime()+".log");
            BufferedWriter writer = new BufferedWriter(new FileWriter(logfile,true));
            writer.append(text+"\n");
            writer.close();
        } catch (IOException ex) {
            Logger.getLogger(logging.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
}
