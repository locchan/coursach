/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Administrator
 */
public class json {
    public static JSONObject parseJSON(String data) throws ParseException{
        JSONParser parser = new JSONParser();
        JSONObject json = (JSONObject) parser.parse(data);
         if(json.get("JSON")!=null && json.get("JSON").toString().equals("BELARUSBANK")){
         return json;
         } else {
             bank.output("INCORRECT JSON",2);
             return null;
         }
    }
    public static String createInfoJSON(String data){
        JSONObject infoJSON = new JSONObject();
        infoJSON.put("JSON","BELARUSBANK");
        infoJSON.put("TYPE", "INFO");
        infoJSON.put("DATA", data);
        return infoJSON.toJSONString();
    }
}
