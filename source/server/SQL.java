/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import static ClientsModule.json.createInfoJSON;
import com.sun.org.apache.xerces.internal.impl.dv.xs.YearMonthDV;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author Administrator
 */
public class SQL {
    public static void sendSQLQueryFromJson(JSONObject json) throws SQLException, IOException{
        if(json==null) return;
        Connection conn = bank.getBankingSQLConnection();
        PreparedStatement prepSt = null;
        switch(json.get("TYPE").toString()){
            case "INSERT":
        prepSt = conn.prepareStatement("INSERT INTO `banking`.`clients`\n" +
            "(`Фамилия`," +
            "`Имя`," +
            "`Отчество`," +
            "`ДатаРождения`," +
            "`Пол`," +
            "`СерияПаспорта`," +
            "`НомерПаспорта`," +
            "`МестоВыдачиПаспорта`," +
            "`ИдентификационныйНомер`," +
            "`МестоРождения`," +
            "`ГородФактическогоПроживания`," +
            "`АдресФактическогоПроживания`," +
            "`ТелефонДомашний`," +
            "`ТелефонМобильный`," +
            "`EMail`," +
            "`МестоРаботы`," +
            "`Должность`," +
            "`ГородПрописки`," +
            "`АдресПрописки`," +
            "`СемейноеПоложение`," +
            "`Гражданство`," +
            "`Инвалидность`," +
            "`Пенсионер`," +
            "`ЕжемесячныйДоход`," +
            "`Военнообязанный`)" +
            "VALUES\n" +
            "(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?);");

        prepSt.setString(1, json.get("LASTNAME").toString());
        prepSt.setString(2, json.get("FIRSTNAME").toString());
        prepSt.setString(3, json.get("PATRONYM").toString());
        prepSt.setDate(4, new Date(Long.parseLong(json.get("DATE").toString())));
        prepSt.setByte(5,  Byte.parseByte(json.get("GENDER").toString()));
        prepSt.setString(6, json.get("PASSPORTSER").toString());
        prepSt.setInt(7, Integer.parseInt(json.get("PASSPORTNUM").toString()));
        prepSt.setString(8, json.get("PLACEOFPASSPORTISSUING").toString());
        prepSt.setString(9, json.get("IDENTIFICNUMBER").toString());
        prepSt.setString(10, json.get("PLACEOFBIRTH").toString());
        prepSt.setString(11, json.get("RESIDENCECITY").toString());
        prepSt.setString(12, json.get("RESIDENCEADDRESS").toString());
        prepSt.setString(13, json.get("PHONEHOME").toString());
        prepSt.setString(14, json.get("PHONEMOBILE").toString());
        prepSt.setString(15, json.get("EMAIL").toString());
        prepSt.setString(16, json.get("WORKPLACE").toString());
        prepSt.setString(17, json.get("WORKPOSITION").toString());
        prepSt.setString(18, json.get("REGISTRATIONCITY").toString());
        prepSt.setString(19, json.get("REGISTRATIONADDRESS").toString());
        prepSt.setString(20, json.get("MARITALSTATUS").toString());
        prepSt.setString(21, json.get("CITIZENSHIP").toString());
        prepSt.setString(22, json.get("DISABILITIES").toString());
        prepSt.setBoolean(23, Boolean.getBoolean(json.get("PENSION").toString()));
        prepSt.setLong(24, Long.parseLong(json.get("MONTLYEARNINGS").toString()));
        prepSt.setBoolean(25, Boolean.getBoolean(json.get("MILITARYSERVICE").toString()));
        bank.output(prepSt.toString(),0);
        prepSt.execute();
        Server.sendData(createInfoJSON("Записано успешно!"));
        break;
            case "FINDTODELETE":
                try {
                String query = createFindSQLQueryFromJSON(json);
                bank.output(query,0);
                prepSt=conn.prepareStatement(query);
                ResultSet rs = prepSt.executeQuery();
                if(!rs.isLast()){
                Server.sendData(convertResultSetToJSONStringAndSendData(rs,"FINDTODELETE"));
                }
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "FIND":
                try {
                String query = createFindSQLQueryFromJSON(json);
                bank.output(query,0);
                prepSt=conn.prepareStatement(query);
                ResultSet rs = prepSt.executeQuery();
                if(!rs.isLast()){
                Server.sendData(convertResultSetToJSONStringAndSendData(rs,"FIND"));
                }
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "UPDATE":
                try {
                String query = createFindSQLQueryFromJSON(json);
                bank.output(query,0);
                prepSt=conn.prepareStatement(query);
                ResultSet rs = prepSt.executeQuery();
                if(!rs.isLast()&&!rs.isFirst()){
                Server.sendData(convertResultSetToJSONStringAndSendData(rs,"UPDATE"));
                }
                } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
        break;
            case "DELETE":
                String query = createDeleteSQLQueryFromJSON(json);
                bank.output(query,0);
                prepSt=conn.prepareStatement(query);
                if(!prepSt.execute()){
            try {
                Server.sendData(createInfoJSON("Entry deleted successfully"));
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                }
                break;
            case "GETLIST":
                try {
                Server.sendData(getListFromSQL(json.get("TABLE").toString(), json.get("COLUMN").toString()));
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "GETAVAILABLEDEPOSITS":
                try {
                Server.sendData(getAvailableDeposits());
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "GETALLCLIENTSINFO":
                try {
                Server.sendData(getAllClientsInfo());
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "SIGNCONTRACT": 
                SignContract(json.toJSONString());
                break;
            case "CREATEACCOUNTS":
                initialDepositAccountingPreparation(Integer.parseInt(json.get("CONTRACTID").toString()));
                break;
            case "CREATECREDITACCOUNTS":
                initialCreditAccountingPreparation(Integer.parseInt(json.get("CONTRACTID").toString()));
                break;
            case "CLOSEDAY":
                Server.sendData(closeDepositAccountingDay());
                break;
            case "CLOSECREDITDAY":
                Server.sendData(closeCreditAccountingDay());
                break;
            case "GETDATE":
                JSONObject jsonObj = new JSONObject();
                jsonObj.put("JSON", "BELARUSBANK");
                jsonObj.put("TYPE", "GETDATE");
                jsonObj.put("DATE","\""+bank.getBankDate()+"\"");
                Server.sendData(jsonObj.toJSONString());
                break;
            case "GETAVAILABLECREDITS":
                try {
                Server.sendData(getAvailableCredits());
            } catch (IOException ex) {
                Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
            }
                break;
            case "SIGNCREDITCONTRACT": 
                SignCreditContract(json.toJSONString());
                break;
            case "LOGIN": 
                JSONObject jsonRep = new JSONObject();
                jsonRep.put("JSON", "BELARUSBANK");
                if(login(json.get("LOGIN").toString(), json.get("PASSWORD").toString())){
                    jsonRep.put("LOGINRESULT","OK");
                } else jsonRep.put("LOGINRESULT","WRONG");
                Server.sendData(jsonRep.toJSONString());
                break;
        }
    }
    public static boolean login(String login, String password){
        try {
            String query = "SELECT * FROM `banking`.`users` WHERE `login`='"+login+"' AND `password`='"+password+"' ;";
            bank.output(query, 0);
            Statement loginSt = bank.getBankingSQLConnection().createStatement();
            ResultSet loginRs = loginSt.executeQuery(query);
            return loginRs.first();
                } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
    public static Date incrementDayIntoDate(Date date){
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.DAY_OF_YEAR, 1);
        return new java.sql.Date(c.getTimeInMillis());
    }
    private static double convert(double amount, String currency){
        double BYNamount = 0;
        switch(currency){
            case "USD":
                BYNamount = amount*2;
            break;
            case "EUR":
                BYNamount = amount*2.39;
            break;
            case "RUB":
                BYNamount = amount*0.0324;
            break;
            case "BYN":
                BYNamount = amount;
            break;
        }
        return BYNamount;
    }
    private static String returnMoney(int contractID){
            String reportText = "";
        try {
            String interestAccount;
            String currentAccount;
            double interestSum;
            double initialAmount;
            //getting contract to find account numbers
            String getDepInfoQuery = "SELECT * FROM `banking`.`deposit_contracts` WHERE id="+contractID;
            bank.output(getDepInfoQuery, 0);
            Statement getDepInfoStmnt = bank.getAccountsSQLConnection().createStatement();
            ResultSet rs = getDepInfoStmnt.executeQuery(getDepInfoQuery);
            rs.first();
            initialAmount = rs.getDouble("depositAmount");
            interestAccount = rs.getString("interestAcc");
            currentAccount = rs.getString("currentAcc");
            String curr = rs.getString("depositCurrency");
            //making entries from interest account into cash
            String getInterestSumQuery = "SELECT SUM(credit) FROM account_deposit"+interestAccount;
            bank.output(getInterestSumQuery, 0);
            Statement getInterestSumStmnt = bank.getAccountsSQLConnection().createStatement();
            ResultSet rs1 = getInterestSumStmnt.executeQuery(getInterestSumQuery);
            rs1.first();
            interestSum = rs1.getDouble(1);
            double convertedInitialAmount = convert(initialAmount, curr);
            makeEntries("account_deposit"+interestAccount, "account_1010_cash", "credit", "debit", interestSum, "BYN");
            reportText+="ПРОВОДКА: на дебет кассы вывелось "+interestSum+"руб. с процентного счёта.\n";
            //making entries from current account into cash
            makeEntries("account_7327_development_fund", "account_deposit"+currentAccount, "credit", "credit", initialAmount, curr);
            reportText+="- на кредит текущего счёта клиента вывелось "+convertedInitialAmount+"руб. из фонда развития.\n";
            makeEntries("account_deposit"+currentAccount, "account_1010_cash", "credit", "debit", initialAmount, curr);
            reportText+="- на дебет кассы вывелось "+convertedInitialAmount+"руб. из текущего счёта клиента.\n";
            makeEntries("account_1010_cash", "", "debit", "", convertedInitialAmount+interestSum, "BYN");
            reportText+=" - из кассы выдано на руки вкладчику "+String.valueOf(convertedInitialAmount+interestSum)+"руб.";
        } catch (SQLException ex) {
            reportText+="ПРОИЗОШЛА ОШИБКА ПРИ ВЫДАЧЕ СРЕДСТВ ВКЛАДЧИКУ";
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return reportText;
    }
    private static boolean checkEndOfMonth(){
            Date issDate = new Date(bank.getBankDate().getTime());
            int month = issDate.getMonth();
            SimpleDateFormat sdf = new SimpleDateFormat("dd");
            int day = Integer.parseInt(sdf.format(issDate));
            int year = issDate.getYear();
            Calendar myCalendar = (Calendar) Calendar.getInstance().clone();
            myCalendar.set(year, month, 1);
            int max_date = myCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
            System.out.println(max_date+" "+day);
            return day==max_date;
    }
    private static int getYearsCount(long startDatelong, long endDatelong){
            Date startDate = new Date(startDatelong);
            int yearStart = startDate.getYear();
            Date endDate = new Date(endDatelong);
            int yearEnd = endDate.getYear();
            return (yearEnd - yearStart);
    }
    private static String closeCreditAccountingDay() throws IOException{
        try {
            JSONObject outpJSON = new JSONObject();
            outpJSON.put("JSON", "BELARUSBANK");
            outpJSON.put("TYPE", "DAYREPORT");
            String dayReport = "";
            Statement bankingSt = bank.getBankingSQLConnection().createStatement();
            String query = "SELECT * FROM `banking`.`credit_contracts` WHERE active=1";
            ResultSet rs = bankingSt.executeQuery(query);
            if(!rs.first()){
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("JSON", "BELARUSBANK");
            jsonResponse.put("TYPE", "DAYREPORT");
            jsonResponse.put("DATA", "");
            return jsonResponse.toJSONString();
            }
            int paymentsLeft;
            int contractID;
            String interestAcc = "account_credit_"+rs.getString("interestAcc");
            String currentAcc = "account_credit_"+rs.getString("currentAcc");
            double monthly_interest_payment = rs.getDouble("monthly_interest_payment");
            double monthly_main_payment = rs.getDouble("monthly_main_payment");
            while(true){
                if(checkEndOfMonth()){
                paymentsLeft = rs.getInt("payments_left");
                contractID = rs.getInt("id");
                dayReport += "Договор №"+contractID+"\n";
                Statement st = bank.getBankingSQLConnection().createStatement();
                paymentsLeft-=1;
                query = "UPDATE `banking`.`credit_contracts` SET  payments_left = "+paymentsLeft+" WHERE id="+contractID;
                st.execute(query);
                //ПРОВОДКИ ПО ПРОЦЕНТНЫМ НАЧИСЛЕНИЯМ
                makeEntries(interestAcc, "account_7327_development_fund", "debit", "credit", monthly_interest_payment, null);
                dayReport+="Начислен месячный платёж по процентам в размере "+monthly_interest_payment+" по договору №"+contractID+"\n";
                makeEntries("","account_1010_cash","","debit", monthly_interest_payment, null);
                dayReport+="Оплачен месячный платёж по процентам в размере "+monthly_interest_payment+" по договору №"+contractID+"\n";
                makeEntries("account_1010_cash",interestAcc,"debit","debit", monthly_interest_payment, null);
                //ПРОВОДКИ ПО ПОГАШЕНИЮ ОСНОВНОГО ДОЛГА
                makeEntries("","account_1010_cash","","debit", monthly_main_payment, null);
                makeEntries("account_1010_cash",currentAcc,"debit","debit", monthly_main_payment, null);
                dayReport+="Оплачен месячный платёж по основному долгу в размере "+monthly_main_payment+" по договору №"+contractID+"\n";
                makeEntries(currentAcc, "account_7327_development_fund","debit","credit", monthly_main_payment, null);
                if((paymentsLeft--)==0){
                dayReport+="Кредитный договор №"+contractID+" окончен.";
                query = "UPDATE `banking`.`credit_contracts` SET  active=0 WHERE id="+contractID;
                st.execute(query);
                }
                }
                if(!rs.next()) break;
            }
            JSONObject jsonResponse = new JSONObject();
            jsonResponse.put("JSON", "BELARUSBANK");
            jsonResponse.put("TYPE", "DAYREPORT");
            jsonResponse.put("DATA", dayReport);
            return jsonResponse.toJSONString();
           
        } catch (SQLException ex) {
            ex.printStackTrace();
            JSONObject outpJSON = new JSONObject();
            outpJSON.put("JSON", "BELARUSBANK");
            outpJSON.put("TYPE", "DAYREPORT");
            outpJSON.put("DATA", "");
            return outpJSON.toJSONString();
        }
    }
    private static String closeDepositAccountingDay() throws IOException{
        try {
            JSONObject outpJSON = new JSONObject();
            outpJSON.put("JSON", "BELARUSBANK");
            outpJSON.put("TYPE", "DAYREPORT");
            String dayReport = "Закрытие банковского дня ("+bank.getBankDate()+")\n";
            int depId;
            String addDayQuery = "INSERT INTO `banking`.`currdate` (date) VALUES(?)";
            PreparedStatement addDaySt = bank.getBankingSQLConnection().prepareStatement(addDayQuery);
            Date newDay = incrementDayIntoDate(bank.getBankDate());
            addDaySt.setDate(1, newDay);
            bank.output(addDaySt.toString(),0);
            addDaySt.execute();
            Statement bankingSt = bank.getBankingSQLConnection().createStatement();
            String query = "SELECT * FROM `banking`.`deposit_contracts` WHERE active=1";
            ResultSet rs = bankingSt.executeQuery(query);
            if(!rs.first()){
            outpJSON.put("JSON", "BELARUSBANK");
            outpJSON.put("TYPE", "DAYREPORT");
            outpJSON.put("DATA", "");
            return outpJSON.toJSONString();
            }
            while(true){
                if(rs.isAfterLast()) break;
                depId = rs.getInt("id");
                String checkQuery = "SELECT * FROM `banking`.`deposit_contracts` WHERE id="+depId+";";
                Statement checkSt = bank.getBankingSQLConnection().createStatement();
                ResultSet checkRs = checkSt.executeQuery(checkQuery);
                checkRs.first();
                if(checkRs.getString("active").equals("0")){
                    rs.next();
                    continue;
                }
                dayReport += "Договор №"+depId+":\n";
                bank.output(bank.getBankDate().toString()+" "+bank.getBankDate().getTime(),0);
                bank.output(rs.getDate("endDate").toString()+" "+rs.getDate("endDate").getTime(), 0);
                if(Long.compare(bank.getBankDate().getTime(), rs.getDate("endDate").getTime())>0){
                    bank.output("Deposit (ID "+rs.getString("id")+ ") has ended.", 0);
                    dayReport += "Договор №"+depId+" истёк:\n";
                    dayReport+=returnMoney(rs.getInt("id"));
                    String updQuery = "UPDATE `banking`.`deposit_contracts` SET active = 0 WHERE id="+rs.getInt("id")+";";
                    Statement updStmnt = bank.getAccountsSQLConnection().createStatement();
                    updStmnt.execute(updQuery);
                    continue;
                }
                String depQuery = "SELECT * FROM `banking`.`available_deposits` WHERE id="+rs.getInt("depositID");
                Statement depSt = bank.getBankingSQLConnection().createStatement();
                ResultSet depStResSet = depSt.executeQuery(depQuery);
                depStResSet.first();
                float interest = depStResSet.getFloat("interest");
                float depositAmount = rs.getFloat("depositAmount");
                float depositInterestForTheDay = (depositAmount*((interest/100)/365));
                String interestAccount = "account_deposit"+rs.getString("interestAcc");
                String curr = rs.getString("depositCurrency");
                makeEntries("account_7327_development_fund", interestAccount, "credit", "credit", depositInterestForTheDay, curr);
                dayReport+="Вкладчику начислено "+depositInterestForTheDay+"руб. процентов на процентный счёт из фонда развития.\n";
                if(!rs.next()) break;
            }
                
            outpJSON.put("DATA", dayReport);
            return outpJSON.toJSONString();
        } catch (SQLException ex) {
            ex.printStackTrace();
            JSONObject outpJSON = new JSONObject();
            outpJSON.put("JSON", "BELARUSBANK");
            outpJSON.put("TYPE", "DAYREPORT");
            outpJSON.put("DATA", "");
             return outpJSON.toJSONString();
        }
    }
    private static void initialCreditAccountingPreparation(int contractID) throws IOException{
        try {
            String query = "SELECT * FROM `banking`.`credit_contracts` WHERE id="+contractID;
            Statement st = bank.getAccountsSQLConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.first();
            int creditId = rs.getInt("creditID");
            double creditAmountDouble = rs.getDouble("creditAmount");
            double creditAmount = rs.getDouble("creditAmount");
            String currentAccountNumber = "account_credit_"+rs.getString("currentAcc");
            String interestAccountNumber = "account_credit_"+rs.getString("interestAcc");
            int yearsCount = getYearsCount(rs.getDate("startDate").getTime(), rs.getDate("endDate").getTime());
            //CREATING ACCOUNT TABLES AND TRIGGERS
            query = getTypicalAccountCreationStatement().replaceAll("`@TABLENAME@`", currentAccountNumber);
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountCreationStatement().replaceAll("`@TABLENAME@`", interestAccountNumber);
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = "INSERT INTO `"+currentAccountNumber+"` (`debit`,`credit`,`saldo`) VALUES (0,0,0)";
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = "INSERT INTO `"+interestAccountNumber+"` (`debit`,`credit`,`saldo`) VALUES (0,0,0)";
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountTriggerCreationStatement().replaceAll("@TABLENAME@", currentAccountNumber).replaceAll("@ACCOUNTNUMBER@", currentAccountNumber.subSequence(15, 19).toString());
            System.out.println(currentAccountNumber.subSequence(15, 19).toString());
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountTriggerCreationStatement().replaceAll("@TABLENAME@", interestAccountNumber).replaceAll("@ACCOUNTNUMBER@", interestAccountNumber.subSequence(15, 19).toString());
            System.out.println(currentAccountNumber.subSequence(15, 19).toString());
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            //ЗАПИСЬ ЕЖЕМЕСЯЧНОГО ПЛАТЕЖА В БАЗУ
            double monthlyPayment = 0;
            query = "SELECT * FROM `banking`.`available_credits` WHERE id="+creditId;
            bank.output(query, 0);
            ResultSet credit = st.executeQuery(query);
            credit.first();
            float interest = (credit.getFloat("interest")/100); 
            double monthly_interest_payment = (interest*creditAmountDouble*yearsCount)/(12*yearsCount);
            double monthly_main_payment = creditAmountDouble/(12*yearsCount);
            
            
            query = "";
            query = "UPDATE `banking`.`credit_contracts` SET  monthly_interest_payment = "+monthly_interest_payment+" WHERE id="+contractID;
            st.execute(query);
            query = "UPDATE `banking`.`credit_contracts` SET  monthly_main_payment = "+monthly_main_payment+" WHERE id="+contractID;
            st.execute(query);
            query = "UPDATE `banking`.`credit_contracts` SET  payments_left = "+(12*yearsCount)+" WHERE id="+contractID;
            st.execute(query);
            //ПЕРВЫЕ ПРОВОДКИ
            makeEntries("account_7327_development_fund", currentAccountNumber, "credit", "debit", creditAmount, null);
            makeEntries(currentAccountNumber, "account_1010_cash",  "debit", "debit", creditAmount, null);
            makeEntries("account_1010_cash", "",  "debit", "", creditAmount, null);
            Server.sendData(json.createInfoJSON("Created accounts for this contract."));
        } catch (SQLException ex) {
            ex.printStackTrace();
           try {
               String query = "DELETE FROM `banking`.`credit_contracts` WHERE id="+contractID;
               Statement st = bank.getAccountsSQLConnection().createStatement();
               st.execute(query);
               Server.sendData(json.createInfoJSON("Something went wrong. Accounts are not created. Contract data is removed."));
               ex.printStackTrace();
            } catch (SQLException ex1) {
                Server.sendData(json.createInfoJSON("Something went terribly wrong. Accounts are not created. Could not remove contract data."));
                ex1.printStackTrace();
            }
        }
     }
    private static void initialDepositAccountingPreparation(int contractID) throws IOException{
        try {
            String query = "SELECT * FROM `banking`.`deposit_contracts` WHERE id="+contractID;
            Statement st = bank.getAccountsSQLConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.first();
            double depositAmount = rs.getDouble("depositAmount");
            String currentAccountNumber = "account_deposit"+rs.getString("currentAcc");
            String interestAccountNumber = "account_deposit"+rs.getString("interestAcc");
            String curr = rs.getString("depositCurrency");
            //CREATING ACCOUNT TABLES AND TRIGGERS
            query = getTypicalAccountCreationStatement().replaceAll("`@TABLENAME@`", currentAccountNumber);
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountCreationStatement().replaceAll("`@TABLENAME@`", interestAccountNumber);
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = "INSERT INTO `"+currentAccountNumber+"` (`debit`,`credit`,`saldo`) VALUES (0,0,0)";
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = "INSERT INTO `"+interestAccountNumber+"` (`debit`,`credit`,`saldo`) VALUES (0,0,0)";
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountTriggerCreationStatement().replaceAll("@TABLENAME@", currentAccountNumber).replaceAll("@ACCOUNTNUMBER@", currentAccountNumber.subSequence(15, 19).toString());
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            query = getTypicalAccountTriggerCreationStatement().replaceAll("@TABLENAME@", interestAccountNumber).replaceAll("@ACCOUNTNUMBER@", interestAccountNumber.subSequence(15, 19).toString());
            System.out.println(currentAccountNumber.subSequence(15, 19).toString());
            bank.output(query.replaceAll("\n", " "), 0);
            st.execute(query);
            //ПЕРВЫЕ ПРОВОДКИ
            makeEntries("account_1010_cash", "", "credit", "", depositAmount, curr);
            makeEntries("account_1010_cash", currentAccountNumber, "debit", "credit", depositAmount, curr);
            makeEntries(currentAccountNumber, "account_7327_development_fund",  "credit", "credit", depositAmount, curr);
            Server.sendData(json.createInfoJSON("Created accounts for this contract."));
        } catch (SQLException ex) {
            try {
                String query = "DELETE FROM `banking`.`deposit_contracts` WHERE id="+contractID;
                Statement st = bank.getAccountsSQLConnection().createStatement();
                st.execute(query);
                Server.sendData(json.createInfoJSON("Something went wrong. Accounts are not created. Contract data is removed."));
                ex.printStackTrace();
            } catch (SQLException ex1) {
                Server.sendData(json.createInfoJSON("Something went terribly wrong. Accounts are not created. Could not remove contract data."));
                ex1.printStackTrace();
            }
        }
    }
    private static void makeEntries(String from, String to, String fromDC, String toDC, double amount, String currency) throws SQLException{
        double BYNamount = 0;
        if(currency!=null){
        BYNamount = convert(amount, currency);
        } else{
        BYNamount = amount;
        }
        System.out.println(from+" "+to+" "+fromDC+" "+toDC);
        Statement st = bank.getAccountsSQLConnection().createStatement();
        String fromDCinversed;
        String query = "";
        if(fromDC.equals("debit")){ fromDCinversed="credit"; }
        else fromDCinversed="debit";
        //getting money out of fromtable
        if(!from.equals("")){
        if(fromDCinversed.equals("debit")){
        query = "INSERT INTO `"+from+"` (`debit`,`credit`) VALUES("+BYNamount+",0);";
        bank.output(query.replaceAll("\n", " "), 0);
        }
        if(fromDCinversed.equals("credit")){
        query = "INSERT INTO `"+from+"` (`debit`,`credit`) VALUES(0,"+BYNamount+");";
        bank.output(query.replaceAll("\n", " "), 0);
        }
        bank.output(query, 0);
        st.execute(query);
        }
        
        if(!to.equals("")){
        //writing money to totable
        if(toDC.equals("debit")){
        query = "INSERT INTO `"+to+"` (`debit`,`credit`) VALUES("+BYNamount+",0);";
        bank.output(query.replaceAll("\n", " "), 0);
        System.out.println(query);
        } else {
        if(toDC.equals("credit")){
        query = "INSERT INTO `"+to+"` (`debit`,`credit`) VALUES(0,"+BYNamount+");";
        bank.output(query.replaceAll("\n", " "), 0);
        System.out.println(query);
        }
        }
        bank.output(query, 0);
        st.execute(query);          
        }
    }
    private static String getTypicalAccountTriggerCreationStatement(){
        return  "CREATE TRIGGER `@TABLENAME@_countSaldo` BEFORE INSERT ON `@TABLENAME@`\n" +
                "FOR EACH ROW\n" +
                "	BEGIN\n" +
                "	 DECLARE debitSum double;\n" +
                "        DECLARE creditSum double;\n" +
                "        DECLARE saldo double;\n" +
                "		set debitSum = (SELECT SUM(debit) from `@TABLENAME@`)+new.debit;\n" +
                "        set creditSum = (SELECT SUM(credit) from `@TABLENAME@`)+new.credit;\n" +
                "        IF((SELECT type FROM banking.account_plan WHERE number=@ACCOUNTNUMBER@) = 'Пассивный') THEN\n" +
                "         set saldo = creditSum - debitSum;\n" +
                "        END IF;\n" +
                "        IF((SELECT type FROM banking.account_plan WHERE number=@ACCOUNTNUMBER@) = 'Активный') THEN\n" +
                "        set saldo = debitSum - creditSum;\n" +
                "        END IF;\n" +
                "        set new.saldo = saldo;\n" +
                "	 END\n";
    }
    private static String getTypicalAccountCreationStatement(){
    return "CREATE TABLE ``@TABLENAME@`` (\n" +
            "  `id` int(11) NOT NULL AUTO_INCREMENT,\n" +
            "  `debit` double DEFAULT NULL,\n" +
            "  `credit` double DEFAULT NULL,\n" +
            "  `saldo` double NOT NULL DEFAULT '0',\n" +
            "  PRIMARY KEY (`id`),\n" +
            "  UNIQUE KEY `id_UNIQUE` (`id`)\n" +
            ") ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;";
    }
    private static void SignContract(String jsonStr){
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObj = (JSONObject) parser.parse(jsonStr);
            String query = "INSERT INTO `banking`.`deposit_contracts`\n" +
                "(`clientID`,\n" +
                "`currentAcc`,\n" +
                "`interestAcc`,\n" +
                "`startDate`,\n" +
                "`endDate`,\n" +
                "`depositID`,\n" +
                "`depositAmount`,\n" +
                "`active`,"+
                "`depositCurrency`)\n" +
                "VALUES(\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n"+
                "?);";
            PreparedStatement stmnt = bank.getBankingSQLConnection().prepareStatement(query);
            stmnt.setInt(1, Integer.parseInt(jsonObj.get("CLIENTID").toString()));
            stmnt.setString(2, jsonObj.get("CURRENTACCOUNT").toString());
            stmnt.setString(3, jsonObj.get("INTERESTACCOUNT").toString());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmnt.setDate(4, new Date(sdf.parse(jsonObj.get("STARTDATE").toString()).getTime()));
            stmnt.setDate(5, new Date(Long.parseLong(jsonObj.get("ENDDATE").toString())));
            stmnt.setInt(6, Integer.parseInt(jsonObj.get("ID").toString()));
            stmnt.setDouble(7, Double.parseDouble(jsonObj.get("DEPOSITAMOUNT").toString()));
            stmnt.setBoolean(8, true);
            stmnt.setString(9, jsonObj.get("DEPOSITCURRENCY").toString());
            bank.output(stmnt.toString().replaceAll("\n", " "), 0);
            stmnt.execute();
            stmnt =  bank.getBankingSQLConnection().prepareStatement("SELECT * FROM `banking`.`deposit_contracts`");
            ResultSet rs  = stmnt.executeQuery();
            rs.last();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CONTRACTID",rs.getInt("id"));
            jsonObject.put("JSON","BELARUSBANK");
            jsonObject.put("MODULE", "DEPOSITS");
            jsonObject.put("TYPE", "CONTRACTSIGNED");
            Server.sendData(jsonObject.toJSONString());
        } catch (ParseException | SQLException | IOException | java.text.ParseException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    private static void SignCreditContract(String jsonStr){
        try {
            JSONParser parser = new JSONParser();
            JSONObject jsonObj = (JSONObject) parser.parse(jsonStr);
            String query = "INSERT INTO `banking`.`credit_contracts`\n" +
                "(`clientID`,\n" +
                "`currentAcc`,\n" +
                "`interestAcc`,\n" +
                "`startDate`,\n" +
                "`endDate`,\n" +
                "`creditID`,\n" +
                "`creditAmount`,\n" +
                "`active`)\n" +
                "VALUES(\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?,\n" +
                "?);";
            PreparedStatement stmnt = bank.getBankingSQLConnection().prepareStatement(query);
            stmnt.setInt(1, Integer.parseInt(jsonObj.get("CLIENTID").toString()));
            stmnt.setString(2, jsonObj.get("CURRENTACCOUNT").toString());
            stmnt.setString(3, jsonObj.get("INTERESTACCOUNT").toString());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            stmnt.setDate(4, new Date(sdf.parse(jsonObj.get("STARTDATE").toString()).getTime()));
            stmnt.setDate(5, new Date(Long.parseLong(jsonObj.get("ENDDATE").toString())));
            stmnt.setInt(6, Integer.parseInt(jsonObj.get("ID").toString()));
            stmnt.setDouble(7, Double.parseDouble(jsonObj.get("CREDITAMOUNT").toString()));
            stmnt.setBoolean(8, true);
            bank.output(stmnt.toString().replaceAll("\n", " "), 0);
            stmnt.execute();
            stmnt =  bank.getBankingSQLConnection().prepareStatement("SELECT * FROM `banking`.`credit_contracts`");
            ResultSet rs  = stmnt.executeQuery();
            rs.last();
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("CONTRACTID",rs.getInt("id"));
            jsonObject.put("JSON","BELARUSBANK");
            jsonObject.put("MODULE", "DEPOSITS");
            jsonObject.put("TYPE", "CONTRACTSIGNED");
            Server.sendData(jsonObject.toJSONString());
        } catch (ParseException | SQLException | IOException | java.text.ParseException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    private static String getAvailableDeposits(){
        ArrayList<JSONObject> deposits = new ArrayList<>();
        try {
            String query = "SELECT * FROM available_deposits";
            Statement st = bank.getBankingSQLConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.first();
            for(int i=0;;i++){
                JSONObject deposit = new JSONObject();
                deposit.put("ID", rs.getString("id"));
                deposit.put("NAME", rs.getString("name"));
                deposit.put("CURRENCY", rs.getString("currency"));
                deposit.put("TYPE", rs.getString("type"));
                deposit.put("TERM", rs.getString("term"));
                deposit.put("MINDEPOSIT", rs.getInt("min_deposit"));
                deposit.put("INTEREST", rs.getFloat("interest"));
                deposits.add(deposit);
                if(!rs.next()) break;
            }
                    } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject depositsJSON = new JSONObject();
        depositsJSON.put("JSON","BELARUSBANK");
        depositsJSON.put("MODULE", "DEPOSITS");
        depositsJSON.put("TYPE", "GETAVAILABLEDEPOSITS");
        for(int i=0;i<deposits.size();i++){
            depositsJSON.put("AVAILABLEDEPOSIT"+i, deposits.get(i).toJSONString());
        }
        return depositsJSON.toJSONString();
    }
    private static String getAvailableCredits(){
        ArrayList<JSONObject> credits = new ArrayList<>();
        try {
            String query = "SELECT * FROM available_credits";
            Statement st = bank.getBankingSQLConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.first();
            for(int i=0;;i++){
                JSONObject credit = new JSONObject();
                credit.put("ID", rs.getString("id"));
                credit.put("NAME", rs.getString("name"));
                credit.put("TERM", rs.getString("term"));
                credit.put("INTEREST", rs.getFloat("interest"));
                credits.add(credit);
                if(!rs.next()) break;
            }
                    } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject depositsJSON = new JSONObject();
        depositsJSON.put("JSON","BELARUSBANK");
        depositsJSON.put("MODULE", "CREDITS");
        depositsJSON.put("TYPE", "GETAVAILABLECREDITS");
        for(int i=0;i<credits.size();i++){
            depositsJSON.put("AVAILABLECREDIT"+i, credits.get(i).toJSONString());
        }
        return depositsJSON.toJSONString();
    }  
    private static String getAllClientsInfo(){
        ArrayList<JSONObject> clients = new ArrayList<>();
        try {
            String query = "SELECT * FROM clients";
            Statement st = bank.getBankingSQLConnection().createStatement();
            ResultSet rs = st.executeQuery(query);
            rs.first();
            for(int i=0;;i++){
                JSONObject client = new JSONObject();
            client.put("ID", rs.getString(26));
            client.put("LASTNAME", rs.getString("Фамилия"));
            client.put("FIRSTNAME", rs.getString("Имя"));
            client.put("PATRONYM", rs.getString("Отчество"));
            client.put("IDENTIFICNUMBER", rs.getString(9));
                clients.add(client);
                if(!rs.next()) break;
            }
                    } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        JSONObject clientsJSON = new JSONObject();
        clientsJSON.put("JSON","BELARUSBANK");
        clientsJSON.put("MODULE", "DEPOSITS");
        clientsJSON.put("TYPE", "GETALLCLIENTSINFO");
        for(int i=0;i<clients.size();i++){
            clientsJSON.put("CLIENT"+i, clients.get(i).toJSONString());
        }
        return clientsJSON.toJSONString();
    }
    private static String createFindSQLQueryFromJSON(JSONObject json){
       String query = "SELECT * FROM `banking`.`clients` WHERE ";
       int count = 0;
        if(!json.get("LASTNAME").toString().equals("")){
            query+="`Фамилия`='"+json.get("LASTNAME")+"'"; count++;
        }
        if(!json.get("FIRSTNAME").toString().equals("")) if(count==0){
            query+="`Имя`='"+json.get("FIRSTNAME")+"'";  count++;
        } else query+=" AND "+"`Имя`='"+json.get("FIRSTNAME")+"'";
        if(!json.get("PATRONYM").toString().equals("") ) if(count==0){
            query+="`Отчество`='"+json.get("PATRONYM")+"'"; count++;
        } else query+=" AND "+"`Отчество`='"+json.get("PATRONYM")+"'";
        if(!json.get("DATE").toString().equals("0") && !json.get("DATE").toString().equals("")) if(count==0){
            query+="`ДатаРождения`='"+json.get("DATE")+"'"; count++;
        } else query+=" AND "+"`ДатаРождения`='"+json.get("DATE")+"'";
        if(!json.get("GENDER").toString().equals(""))  if(count==0){
            query+="`Пол`='"+json.get("GENDER")+"'"; count++;
        } else query+=" AND "+"`Пол`='"+json.get("GENDER")+"'";
        if(!json.get("PASSPORTSER").toString().equals(""))  if(count==0){
            query+="`СерияПаспорта`='"+json.get("PASSPORTSER")+"'"; count++;
        } else query+=" AND "+"`СерияПаспорта`='"+json.get("PASSPORTSER")+"'";
        if(!json.get("PASSPORTNUM").toString().equals("0") && !json.get("PASSPORTNUM").toString().equals(""))  if(count==0){
            query+="`НомерПаспорта`='"+json.get("PASSPORTNUM")+"'"; count++;
        } else query+=" AND "+"`НомерПаспорта`='"+json.get("PASSPORTNUM")+"'";
        if(!json.get("PLACEOFPASSPORTISSUING").toString().equals(""))  if(count==0){
            query+="`МестоВыдачиПаспорта`='"+json.get("PLACEOFPASSPORTISSUING")+"'"; count++;
        } else query+=" AND "+"`МестоВыдачиПаспорта`='"+json.get("PLACEOFPASSPORTISSUING")+"'";
        if(!json.get("IDENTIFICNUMBER").toString().equals(""))  if(count==0){
            query+="`ИдентификационныйНомер`='"+json.get("IDENTIFICNUMBER")+"'"; count++;
        } else query+=" AND "+"`ИдентификационныйНомер`='"+json.get("IDENTIFICNUMBER")+"'";
        if(!json.get("PLACEOFBIRTH").toString().equals(""))  if(count==0){
            query+="`МестоРождения`='"+json.get("PLACEOFBIRTH")+"'"; count++;
        } else query+=" AND "+"`МестоРождения`='"+json.get("PLACEOFBIRTH")+"'";
        if(!json.get("RESIDENCECITY").toString().equals(""))  if(count==0){
            query+="`ГородФактическогоПроживания`='"+json.get("RESIDENCECITY")+"'"; count++;
        } else query+=" AND "+"`ГородФактическогоПроживания`='"+json.get("RESIDENCECITY")+"'";
        if(!json.get("RESIDENCEADDRESS").toString().equals(""))  if(count==0){
            query+="`АдресФактическогоПроживания`='"+json.get("RESIDENCEADDRESS")+"'"; count++;
        } else query+=" AND "+"`АдресФактическогоПроживания`='"+json.get("RESIDENCEADDRESS")+"'";
        if(!json.get("PHONEHOME").toString().equals(""))  if(count==0){
            query+="`ТелефонДомашний`='"+json.get("PHONEHOME")+"'"; count++;
        } else query+=" AND "+"`ТелефонДомашний`='"+json.get("PHONEHOME")+"'";
        if(!json.get("PHONEMOBILE").toString().equals(""))  if(count==0){
            query+="`ТелефонМобильный`='"+json.get("PHONEMOBILE")+"'"; count++;
        } else query+=" AND "+"`ТелефонМобильный`='"+json.get("PHONEMOBILE")+"'";
        if(!json.get("EMAIL").toString().equals(""))  if(count==0){
            query+="`EMail`='"+json.get("EMAIL")+"'"; count++;
        } else query+=" AND "+"`EMail`='"+json.get("EMAIL")+"'";
        if(!json.get("WORKPLACE").toString().equals(""))  if(count==0){
            query+="`МестоРаботы`='"+json.get("WORKPLACE")+"'"; count++;
        } else query+=" AND "+"`МестоРаботы`='"+json.get("WORKPLACE")+"'";
        if(!json.get("WORKPOSITION").toString().equals(""))  if(count==0){
            query+="`Должность`='"+json.get("WORKPOSITION")+"'"; count++;
        } else query+=" AND "+"`Должность`='"+json.get("WORKPOSITION")+"'";
        if(!json.get("REGISTRATIONCITY").toString().equals(""))  if(count==0){
            query+="`ГородПрописки`='"+json.get("REGISTRATIONCITY")+"'"; count++;
        } else query+=" AND "+"`ГородПрописки`='"+json.get("REGISTRATIONCITY")+"'";
        if(!json.get("REGISTRATIONADDRESS").toString().equals(""))  if(count==0){
            query+="`АдресПрописки`='"+json.get("REGISTRATIONADDRESS")+"'"; count++;
        } else query+=" AND "+"`АдресПрописки`='"+json.get("REGISTRATIONADDRESS")+"'";
        if(!json.get("MARITALSTATUS").toString().equals(""))  if(count==0){
            query+="`СемейноеПоложение`='"+json.get("MARITALSTATUS")+"'"; count++;
        } else query+=" AND "+"`СемейноеПоложение`='"+json.get("MARITALSTATUS")+"'";
        if(!json.get("CITIZENSHIP").toString().equals(""))  if(count==0){
            query+="`Гражданство`='"+json.get("CITIZENSHIP")+"'"; count++;
        } else query+=" AND "+"`Гражданство`='"+json.get("CITIZENSHIP")+"'";
        if(!json.get("DISABILITIES").toString().equals(""))  if(count==0){
            query+="`Инвалидность`='"+json.get("DISABILITIES")+"'"; count++;
        } else query+=" AND "+"`Инвалидность`='"+json.get("DISABILITIES")+"'";
        if(!json.get("PENSION").toString().equals(""))  if(count==0){
            query+="`Пенсионер`='"+json.get("PENSION")+"'"; count++;
        } else query+=" AND "+"`Пенсионер`='"+json.get("PENSION")+"'";
        if(!json.get("MONTLYEARNINGS").toString().equals("0") && !json.get("MONTLYEARNINGS").toString().equals("")) if(count==0){
            query+="`ЕжемесячныйДоход`='"+json.get("MONTLYEARNINGS")+"'"; count++;
        } else query+=" AND "+"`ЕжемесячныйДоход`='"+json.get("MONTLYEARNINGS")+"'";
        if(!json.get("MILITARYSERVICE").toString().equals("")) if(count==0){
            query+="`Военнообязанный`='"+json.get("MILITARYSERVICE")+"'"; count++;
        } else query+=" AND "+"`Военнообязанный`='"+json.get("MILITARYSERVICE")+"'";
        return query;
    }
    private static String createDeleteSQLQueryFromJSON(JSONObject json){
       String query = "DELETE FROM `banking`.`clients` WHERE ";
       int count = 0;
        if(!json.get("IDENTIFICNUMBER").toString().equals("")){
            query+="`ИдентификационныйНомер`='"+json.get("IDENTIFICNUMBER")+"'";
             return query;
        }
        if(!json.get("LASTNAME").toString().equals("")){
            query+="`Фамилия`='"+json.get("LASTNAME")+"'"; count++;
        }
        if(!json.get("FIRSTNAME").toString().equals("")) if(count==0){
            query+="`Имя`='"+json.get("FIRSTNAME")+"'";  count++;
        } else query+=" AND "+"`Имя`='"+json.get("FIRSTNAME")+"'";
        if(!json.get("PATRONYM").toString().equals("") ) if(count==0){
            query+="`Отчество`='"+json.get("PATRONYM")+"'";
        } else query+=" AND "+"`Отчество`='"+json.get("PATRONYM")+"'";
        if(!json.get("DATE").toString().equals("0") && !json.get("DATE").toString().equals("")) if(count==0){
            query+="`ДатаРождения`='"+json.get("DATE").toString().replaceAll("\"", "")+"'";
        } else query+=" AND "+"`ДатаРождения`='"+json.get("DATE").toString().replaceAll("\"", "")+"'";
        if(!json.get("GENDER").toString().equals(""))  if(count==0){
            query+="`Пол`='"+json.get("GENDER")+"'";
        } else query+=" AND "+"`Пол`='"+json.get("GENDER")+"'";
        if(!json.get("PASSPORTSER").toString().equals(""))  if(count==0){
            query+="`СерияПаспорта`='"+json.get("PASSPORTSER")+"'";
        } else query+=" AND "+"`СерияПаспорта`='"+json.get("PASSPORTSER")+"'";
        if(!json.get("PASSPORTNUM").toString().equals("0") && !json.get("PASSPORTNUM").toString().equals(""))  if(count==0){
            query+="`НомерПаспорта`='"+json.get("PASSPORTNUM")+"'";
        } else query+=" AND "+"`НомерПаспорта`='"+json.get("PASSPORTNUM")+"'";
        if(!json.get("PLACEOFPASSPORTISSUING").toString().equals(""))  if(count==0){
            query+="`МестоВыдачиПаспорта`='"+json.get("PLACEOFPASSPORTISSUINR")+"'";
        } else query+=" AND "+"`МестоВыдачиПаспорта`='"+json.get("PLACEOFPASSPORTISSUING")+"'";
        if(!json.get("IDENTIFICNUMBER").toString().equals(""))  if(count==0){
            query+="`ИдентификационныйНомер`='"+json.get("IDENTIFICNUMBER")+"'";
        } else query+=" AND "+"`ИдентификационныйНомер`='"+json.get("IDENTIFICNUMBER")+"'";
        if(!json.get("PLACEOFBIRTH").toString().equals(""))  if(count==0){
            query+="`МестоРождения`='"+json.get("PLACEOFBIRTH")+"'";
        } else query+=" AND "+"`МестоРождения`='"+json.get("PLACEOFBIRTH")+"'";
        if(!json.get("RESIDENCECITY").toString().equals(""))  if(count==0){
            query+="`ГородФактическогоПроживания`='"+json.get("RESIDENCECITY")+"'";
        } else query+=" AND "+"`ГородФактическогоПроживания`='"+json.get("RESIDENCECITY")+"'";
        if(!json.get("RESIDENCEADDRESS").toString().equals(""))  if(count==0){
            query+="`АдресФактическогоПроживания`='"+json.get("RESIDENCEADDRESS")+"'";
        } else query+=" AND "+"`АдресФактическогоПроживания`='"+json.get("RESIDENCEADDRESS")+"'";
        if(!json.get("PHONEHOME").toString().equals(""))  if(count==0){
            query+="`ТелефонДомашний`='"+json.get("PHONEHOME")+"'";
        } else query+=" AND "+"`ТелефонДомашний`='"+json.get("PHONEHOME")+"'";
        if(!json.get("PHONEMOBILE").toString().equals(""))  if(count==0){
            query+="`ТелефонМобильный`='"+json.get("PHONEMOBILE")+"'";
        } else query+=" AND "+"`ТелефонМобильный`='"+json.get("PHONEMOBILE")+"'";
        if(!json.get("EMAIL").toString().equals(""))  if(count==0){
            query+="`EMail`='"+json.get("EMAIL")+"'";
        } else query+=" AND "+"`EMail`='"+json.get("EMAIL")+"'";
        if(!json.get("WORKPLACE").toString().equals(""))  if(count==0){
            query+="`МестоРаботы`='"+json.get("WORKPLACE")+"'";
        } else query+=" AND "+"`МестоРаботы`='"+json.get("WORKPLACE")+"'";
        if(!json.get("WORKPOSITION").toString().equals(""))  if(count==0){
            query+="`Должность`='"+json.get("WORKPOSITION")+"'";
        } else query+=" AND "+"`Должность`='"+json.get("WORKPOSITION")+"'";
        if(!json.get("REGISTRATIONCITY").toString().equals(""))  if(count==0){
            query+="`ГородПрописки`='"+json.get("REGISTRATIONCITY")+"'";
        } else query+=" AND "+"`ГородПрописки`='"+json.get("REGISTRATIONCITY")+"'";
        if(!json.get("REGISTRATIONADDRESS").toString().equals(""))  if(count==0){
            query+="`АдресПрописки`='"+json.get("REGISTRATIONADDRESS")+"'";
        } else query+=" AND "+"`АдресПрописки`='"+json.get("REGISTRATIONADDRESS")+"'";
        if(!json.get("MARITALSTATUS").toString().equals(""))  if(count==0){
            query+="`СемейноеПоложение`='"+json.get("MARITALSTATUS")+"'";
        } else query+=" AND "+"`СемейноеПоложение`='"+json.get("MARITALSTATUS")+"'";
        if(!json.get("CITIZENSHIP").toString().equals(""))  if(count==0){
            query+="`Гражданство`='"+json.get("CITIZENSHIP")+"'";
        } else query+=" AND "+"`Гражданство`='"+json.get("CITIZENSHIP")+"'";
        if(!json.get("DISABILITIES").toString().equals(""))  if(count==0){
            query+="`Инвалидность`='"+json.get("DISABILITIES")+"'";
        } else query+=" AND "+"`Инвалидность`='"+json.get("DISABILITIES")+"'";
        if(!json.get("PENSION").toString().equals(""))  if(count==0){
            query+="`Пенсионер`='"+json.get("PENSION")+"'";
        } else query+=" AND "+"`Пенсионер`='"+json.get("PENSION")+"'";
        if(!json.get("MONTLYEARNINGS").toString().equals("0") && !json.get("MONTLYEARNINGS").toString().equals("")) if(count==0){
            query+="`ЕжемесячныйДоход`='"+json.get("MONTLYEARNINGS")+"'";
        } else query+=" AND "+"`ЕжемесячныйДоход`='"+json.get("MONTLYEARNINGS")+"'";
        if(!json.get("MILITARYSERVICE").toString().equals("")) if(count==0){
            query+="`Военнообязанный`='"+json.get("MILITARYSERVICE")+"'";
        } else query+=" AND "+"`Военнообязанный`='"+json.get("MILITARYSERVICE")+"'";
        return query;
    }
    private static String convertResultSetToJSONStringAndSendData(ResultSet rs, String respto) throws SQLException, IOException{
            JSONObject responce = new JSONObject();
            if(!rs.first()){
            return json.createInfoJSON("Таких записей нет!");
            }
            int i = 0;
            while(true){
            JSONObject resp = new JSONObject();
            i++;
            resp.put("LASTNAME", rs.getString(1));
            resp.put("FIRSTNAME", rs.getString(2));
            resp.put("PATRONYM", rs.getString(3));
            resp.put("DATE", "\""+rs.getDate(4)+"\"");
            resp.put("GENDER", rs.getBoolean(5));
            resp.put("PASSPORTSER", rs.getString(6));
            resp.put("PASSPORTNUM", rs.getInt(7));
            resp.put("PLACEOFPASSPORTISSUING", rs.getString(8));
            resp.put("IDENTIFICNUMBER", rs.getString(9));
            resp.put("PLACEOFBIRTH", rs.getString(10));
            resp.put("RESIDENCECITY", rs.getString(11));
            resp.put("RESIDENCEADDRESS",rs.getString(12));
            resp.put("PHONEHOME", rs.getString(13));
            resp.put("PHONEMOBILE", rs.getString(14));
            resp.put("EMAIL", rs.getString(15));
            resp.put("WORKPLACE", rs.getString(16));
            resp.put("WORKPOSITION", rs.getString(17));
            resp.put("REGISTRATIONCITY", rs.getString(18));
            resp.put("REGISTRATIONADDRESS", rs.getString(19));
            resp.put("MARITALSTATUS", rs.getString(20));
            resp.put("CITIZENSHIP", rs.getString(21));
            resp.put("DISABILITIES", rs.getString(22));
            resp.put("PENSION", rs.getBoolean(23));
            resp.put("MONTLYEARNINGS", rs.getInt(24));
            resp.put("MILITARYSERVICE", rs.getBoolean(25));
            responce.put("JSON","BELARUSBANK");
            responce.put("TYPE", "DATA");
            responce.put("RESPONCETO",respto);
            responce.put("RESULT #"+i,resp.toJSONString());
            if(!rs.next()){
               break;
            }
            }
            return responce.toJSONString();
    }
    private static String getListFromSQL(String table, String colname){
        JSONObject jsonObj = new JSONObject();
        jsonObj.put("JSON", "BELARUSBANK");
        jsonObj.put("TYPE", "LIST");
        jsonObj.put("RESPONCETO", "GETLIST");
        jsonObj.put("TABLE", table);
        try {
            String query = "SELECT "+colname+" FROM "+table;
            Connection conn = bank.getBankingSQLConnection();
            ResultSet rs = conn.prepareStatement(query).executeQuery();
            rs.first();
            for(int i=0;;i++){
                jsonObj.put("RESULT"+i, rs.getString(colname));
                if(!rs.next()) break;
            }
        } catch (SQLException ex) {
            Logger.getLogger(SQL.class.getName()).log(Level.SEVERE, null, ex);
        }
        return jsonObj.toJSONString();
    }
}
