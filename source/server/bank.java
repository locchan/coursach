/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package server;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import utils.logging;

/**
 *
 * @author Administrator
 */
public class bank {
    private static Date currentDate;
    private static String startDateTime = "";
    private static Connection BankingSQLConnection = null;
    private static Connection AccountsSQLConnection = null;
    private static Calendar cal = Calendar.getInstance();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.YYYY HH:mm:ss");
    public static void main(String[] args) throws SQLException{
        SimpleDateFormat StartDateFormat = new SimpleDateFormat("dd.MM.YYYY_HH.mm.ss");
        startDateTime = StartDateFormat.format(cal.getTime());
        Server.startServer();
        getBankingSQLConnection();
        getAccountsSQLConnection();
        output("Belarusbank server started", 0);
    }
    public static Date getBankDate(){
        try {
            Statement st = getBankingSQLConnection().createStatement();
            ResultSet rs = st.executeQuery("SELECT * FROM currdate");
            if(!rs.last()){
                output("INSERT INTO currdate (date) VALUES(?)",0);
                PreparedStatement stm = getBankingSQLConnection().prepareStatement("INSERT INTO currdate (date) VALUES(?)");
                stm.setDate(1, new Date(System.currentTimeMillis()));
                stm.execute();
                return new Date(System.currentTimeMillis());
            }
            currentDate = rs.getDate("date");
        } catch (SQLException ex) {
            Logger.getLogger(bank.class.getName()).log(Level.SEVERE, null, ex);
        }
            return currentDate;
    }
    public static Connection getBankingSQLConnection() {
        if(BankingSQLConnection==null){
            try {
                BankingSQLConnection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/banking","root", "Pa$$w0rd");
            } catch (SQLException e) {
                Logger.getLogger(bank.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return BankingSQLConnection;
    }
    public static Connection getAccountsSQLConnection() {
        if(AccountsSQLConnection==null){
            try {
                AccountsSQLConnection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/accounts","root", "Pa$$w0rd");
            } catch (SQLException e) {
                Logger.getLogger(bank.class.getName()).log(Level.SEVERE, null, e);
            }
        }
        return AccountsSQLConnection;
    }
    public static String getStartDateTime() {
        return startDateTime;
    }
    public static void output(String text, int severity){
        cal = Calendar.getInstance();
        switch(severity){
            case 0:
                System.out.println(sdf.format(cal.getTime())+" INFO: "+text);
                logging.log(sdf.format(cal.getTime())+" INFO: "+text);
                break;
            case 1:
                System.out.println(sdf.format(cal.getTime())+" WARN: "+text);
                logging.log(sdf.format(cal.getTime())+" WARN: "+text);
                break;
            case 2:
                System.err.println(sdf.format(cal.getTime())+" ERR: "+text);
                logging.log(sdf.format(cal.getTime())+" ERR: "+text);
                break;
            default:
                System.err.print(sdf.format(cal.getTime())+" MESSAGE WITH INVALID SEVERITY: "+text);
                logging.log(sdf.format(cal.getTime())+" MESSAGE WITH INVALID SEVERITY: "+text);
                break;
    }
    }

}
