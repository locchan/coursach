/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
package ClientsModule;
/**
 *
 * @author Administrator
*/
package server;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.parser.ParseException;

public class Server {
   private static Socket socket = new Socket();
   private static Thread t = new Thread();
        private static void serverListener() throws IOException{
        ServerSocket serverSocket = new ServerSocket(37529);
        socket.setKeepAlive(true);
        bank.output("Server socket initialized and waiting for connections", 0);
        socket = serverSocket.accept();
        bank.output("Client started data transferring ("+socket.getRemoteSocketAddress()+")", 0);
        BufferedReader inpReader;
        String line;
         while(true) {
            try {
                inpReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                line = inpReader.readLine();
                bank.output("RECIEVED: "+line,0);
                SQL.sendSQLQueryFromJson(json.parseJSON(line));
            } catch (ParseException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            } catch (SQLException ex) {
                bank.output(ex.getLocalizedMessage(), 1);
                Server.sendData(json.createInfoJSON(ex.getLocalizedMessage()));
            }
            socket = serverSocket.accept();
            bank.output("Client started data transferring ("+socket.getRemoteSocketAddress()+")", 0);
        }         
}
        public static void startServer(){
        t = new Thread(() -> {
            try {
                serverListener();
            } catch (IOException ex) {
                Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            }
        });
        t.start();
        }
        public static void sendData(String data) throws IOException{
            if(socket.isConnected()){
                BufferedWriter outpWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
                outpWriter.write(data+"\n");
                bank.output("SENDING: "+data,0);
                outpWriter.flush();
                outpWriter.close();
            } else bank.output("CANNOT SEND DATA: SOCKET IS CLOSED", 2);
        }
}
