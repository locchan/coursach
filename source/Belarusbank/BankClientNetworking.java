/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Belarusbank;

import DepositsModule.*;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author Administrator
 */
public class BankClientNetworking {
        private static Socket socket = new Socket();
        public static void sendData(String data, UI gui) throws IOException, ParseException{
           socket = new Socket("localhost", 37529);
            if(socket.isConnected()){
               try {
                   BufferedWriter outpWriter = new BufferedWriter
                        (new OutputStreamWriter(socket.getOutputStream()));
                   BufferedReader inpReader = new BufferedReader
                        (new InputStreamReader(socket.getInputStream()));
                   outpWriter.write(data+"\n");
                   System.out.println("SENDING: "+data+"\n");
                   outpWriter.flush();
                   String inputData = inpReader.readLine();
                   System.out.println("RECIEVED: "+inputData+"\n");
                   JSONParser parser = new JSONParser();
                   JSONObject obj = (JSONObject) parser.parse(inputData);
                   gui.getConsole().append(obj.get("DATA").toString()+"=====================");
                   inpReader.close();
                   outpWriter.close();
               } catch (org.json.simple.parser.ParseException ex) {
                   Logger.getLogger(BankClientNetworking.class.getName()).log(Level.SEVERE, null, ex);
               }
            } else System.err.println("CANNOT SEND DATA: SOCKET IS CLOSED");
        }
         public static JSONObject sendDataAndGetResponse(String data, UI gui) throws IOException, ParseException{
           socket = new Socket("localhost", 37529);
            if(socket.isConnected()){
               try {
                   BufferedWriter outpWriter = new BufferedWriter
                        (new OutputStreamWriter(socket.getOutputStream()));
                   BufferedReader inpReader = new BufferedReader
                        (new InputStreamReader(socket.getInputStream()));
                   outpWriter.write(data+"\n");
                   System.out.println("SENDING: "+data+"\n");
                   outpWriter.flush();
                   String inputData = inpReader.readLine();
                   System.out.println("RECIEVED: "+inputData+"\n");
                   JSONParser parser = new JSONParser();
                   JSONObject obj = (JSONObject) parser.parse(inputData);
                   inpReader.close();
                   outpWriter.close();
                   if(obj.get("JSON").toString().equals("BELARUSBANK")){
                    return obj;
                   } else return null;
               } catch (org.json.simple.parser.ParseException ex) {
                   Logger.getLogger(BankClientNetworking.class.getName()).log(Level.SEVERE, null, ex);
               }
            } else System.err.println("CANNOT SEND DATA: SOCKET IS CLOSED");
            return null;
        }
}
