/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Belarusbank;

import java.io.IOException;
import java.text.ParseException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.simple.JSONObject;

/**
 *
 * @author Administrator
 */
public class login {
    public static boolean login(String login, String password){
        try {
            JSONObject loginJson = new JSONObject();
            loginJson.put("JSON","BELARUSBANK");
            loginJson.put("TYPE","LOGIN");
            loginJson.put("LOGIN",login);
            loginJson.put("PASSWORD",password);
            JSONObject response = BankClientNetworking.sendDataAndGetResponse(loginJson.toJSONString(), null);
            return (response.get("LOGINRESULT").equals("OK"));
        } catch (IOException | ParseException ex) {
            Logger.getLogger(login.class.getName()).log(Level.SEVERE, null, ex);
        }
        return false;
    }
}
