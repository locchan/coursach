/*

 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientsModule;

import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * author Administrator
 */
public class json {
    public static String createJSON(String jsontype, 
            String lastName, String firstName, String patronym, Date date, String gender,
            String passportSer, int passportNum, String placeOfIssuing, String identificNumber,
            String placeOfBirth, String residenceCity, String residenceAddress, String phoneHome,
            String phoneMobile, String Email, String workPlace, String workPosition,
            String registrationCity, String registrationAddress, String maritalStatus,
            String citizenship, String disabilities, String pension, String monthlyEarnings,
            String militaryService){
            JSONObject client = new JSONObject();
            client.put("JSON","BELARUSBANK");
            client.put("TYPE", jsontype);
            client.put("MODULE", "CLIENTS");
            client.put("LASTNAME", lastName);
            client.put("FIRSTNAME", firstName);
            client.put("PATRONYM", patronym);
            client.put("DATE", date.getTime());
            client.put("GENDER", gender);
            client.put("PASSPORTSER", passportSer);
            client.put("PASSPORTNUM", passportNum);
            client.put("PLACEOFPASSPORTISSUING", placeOfIssuing);
            client.put("IDENTIFICNUMBER", identificNumber);
            client.put("PLACEOFBIRTH", placeOfBirth);
            client.put("RESIDENCECITY", residenceCity);
            client.put("RESIDENCEADDRESS",residenceAddress);
            client.put("PHONEHOME", phoneHome);
            client.put("PHONEMOBILE", phoneMobile);
            client.put("EMAIL", Email);
            client.put("WORKPLACE", workPlace);
            client.put("WORKPOSITION", workPosition);
            client.put("REGISTRATIONCITY", registrationCity);
            client.put("REGISTRATIONADDRESS", registrationAddress);
            client.put("MARITALSTATUS", maritalStatus);
            client.put("CITIZENSHIP", citizenship);
            client.put("DISABILITIES", disabilities);
            client.put("PENSION", pension);
            client.put("MONTLYEARNINGS", monthlyEarnings);
            client.put("MILITARYSERVICE", militaryService);
            return client.toJSONString();
    }
    public static String createJSON(String jsontype, String info1, String info2){
        JSONObject jsonObj = new JSONObject();
        switch(jsontype){
            case "GETLIST":
            jsonObj.put("JSON","BELARUSBANK");
            jsonObj.put("TYPE", jsontype);
            jsonObj.put("MODULE", "CLIENTS");
            jsonObj.put("TABLE", info1);
            jsonObj.put("COLUMN", info2);
            return jsonObj.toJSONString();
        }
        return null;
    }
    public static String createInfoJSON(String data){
        JSONObject infoJSON = new JSONObject();
        infoJSON.put("JSON","BELARUSBANK");
        infoJSON.put("TYPE", "INFO");
        infoJSON.put("MODULE", "CLIENTS");
        infoJSON.put("DATA", data);
        return infoJSON.toJSONString();
    }
    public static void parseJSON(String data, UI gui) throws java.text.ParseException{
        try {
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject) parser.parse(data);
            if(json.get("JSON").toString().equals("BELARUSBANK")){
                switch(json.get("TYPE").toString()){
                    case "INFO":
                    if(gui==null){
                         return;
                         }
                        gui.getConsole().append("Информация от сервера: "+json.get("DATA").toString()+"\n");
                        System.out.println("INFO FROM SERVER: "+json.get("DATA").toString());
                        if(json.get("DATA").toString().contains("Записано успешно!")){
                         gui.getWrite().setEnabled(true);
                         gui.getFind().setEnabled(true);
                         gui.getDelete().setEnabled(true);
                        }
                        break;
                    case "DATA":
                        if(gui==null){
                         return;
                         }
                        System.out.println("DATA FROM SERVER: "+json.toJSONString());
                switch (json.get("RESPONCETO").toString()) {
                    case "FIND":
                        gui.getConsole().append(getJSONResults(json,"FIND",gui));
                        break; 
                    case "UPDATE":
                        gui.getConsole().append(getJSONResults(json,"UPDATE",gui));
                        break; 
                    case "FINDTODELETE":
                        gui.getConsole().append(getJSONResults(json,"FINDTODELETE",gui));
                        break;
                    default:
                        break;
                }
                        break;
                    case "LIST":
                        System.out.println("GOT A LIST FROM SERVER.");
                        switch(json.get("TABLE").toString()){
                            case "marital_status":
                            UI.addMaritalStatus(getListFromJSON(json));
                            break;
                            case "disability":
                            UI.addDisabilities(getListFromJSON(json));
                            break;
                            case "countries":
                            UI.addCountries(getListFromJSON(json));
                            break;
                        }
                        break;
                    default:
                        System.out.println("INCORRECT JSON");
                        break;
                }
            } else{
                System.out.println("INCORRECT JSON");
            }
        } catch (ParseException ex) {
            gui.getConsole().append(ex.getLocalizedMessage());
            System.out.println("INCORRECT JSON");
            ex.printStackTrace();
        }
    }
    public static ArrayList<String> getListFromJSON(JSONObject jsonObj){
        ArrayList<String> results = new ArrayList();
       for(int i=0;;i++){
           if(jsonObj.get("RESULT"+i)!=null){
               results.add(jsonObj.get("RESULT"+i).toString());
           } else break;
       }
       return results;
    }
    public static String getJSONResults(JSONObject json, String type, UI gui) throws ParseException, java.text.ParseException{
        String text = "";
        switch (type) {
            case "FIND":
            {
                int i = 0;
                while(true){
                    if(json.get("RESULT #"+(i+1))!=null){
                        JSONParser parser = new JSONParser();
                        System.out.println(json.get(("RESULT #"+(i+1))));
                        String resultStr = json.get(("RESULT #"+(i+1))).toString();
                        JSONObject result = (JSONObject) parser.parse(resultStr);
                        text+="=============="+"Клиент #"+(i+1)+"==============\n";
                        text+="Фамилия: "+result.get("LASTNAME").toString()+"\n";
                        text+="Имя: "+result.get("FIRSTNAME").toString()+"\n";
                        text+="Отчество: "+result.get("PATRONYM").toString()+"\n";
                        text+="Дата рождения: "+result.get("DATE").toString()+"\n";
                        text+="Гражданство: "+result.get("CITIZENSHIP").toString()+"\n";
                        text+="Место рождения: "+result.get("PLACEOFBIRTH").toString()+"\n";
                        text+="Город фактического проживания: "+result.get("RESIDENCECITY").toString()+"\n";
                        text+="Адрес фактического проживания: "+result.get("RESIDENCEADDRESS").toString()+"\n";
                        text+="Пол: "+result.get("GENDER").toString()+"\n";
                        text+="Серия паспорта: "+result.get("PASSPORTSER").toString()+"\n";
                        text+="Номер паспорта: "+result.get("PASSPORTNUM").toString()+"\n";
                        text+="Место выдачи паспорта: "+result.get("PLACEOFPASSPORTISSUING").toString()+"\n";
                        text+="Идентификационный номер: "+result.get("IDENTIFICNUMBER").toString()+"\n";
                        text+="Телефон домашний: "+result.get("PHONEHOME").toString()+"\n";
                        text+="Мобильный телефон: "+result.get("PHONEMOBILE").toString()+"\n";
                        text+="E-mail: "+result.get("EMAIL").toString()+"\n";
                        text+="Место работы: "+result.get("WORKPLACE").toString()+"\n";
                        text+="Должность: "+result.get("WORKPOSITION").toString()+"\n";
                        text+="Город прописки: "+result.get("REGISTRATIONCITY").toString()+"\n";
                        text+="Адрес прописки: "+result.get("REGISTRATIONADDRESS").toString()+"\n";
                        text+="Семейное положение: "+result.get("MARITALSTATUS").toString()+"\n";
                        text+="Инвалидность: "+result.get("DISABILITIES").toString()+"\n";
                        text+="Пенсионер: "+result.get("PENSION").toString()+"\n";
                        text+="Ежемесячный доход: "+result.get("MONTLYEARNINGS").toString()+"\n";
                        text+="Военнообязанный: "+result.get("MILITARYSERVICE").toString()+"\n";
                        text = text.replaceAll(": true",": Да");
                        text = text.replaceAll(": false",": Нет");
                        text = text.replaceAll("Пол: Да","Пол: Мужской");
                        text = text.replaceAll("Пол: Нет","Пол: Женский");
                    } else break;
                    i++;
                }
                return text;
            }
            case "UPDATE":
                {
                    int i = 0;
                    while(true){
                        if(json.get("RESULT #"+(i+1))!=null){
                            i++;
                        } else break;
                    }           if(i==1){
                        JSONParser parser = new JSONParser();
                        String resultStr = json.get(("RESULT #1")).toString();
                        JSONObject result = (JSONObject) parser.parse(resultStr);
                        insertJSONIntoGUI(result, gui);
                    } else gui.getConsole().append("По вашему запросу обнаружено несколко клиентов. Укажите больше данных: \n" + getJSONResults(json, "FIND",gui));
                    break;
                }
            case "FINDTODELETE":
                {
                    int i = 0;
                    while(true){
                        if(json.get("RESULT #"+(i+1))!=null){
                            i++;
                        } else break;
                    }           if(i==1){
                        
                        JSONParser parser = new JSONParser();
                        String resultStr = json.get(("RESULT #1")).toString().replaceAll(":true", ":1").replaceAll(":false", ":0");
                        JSONObject jsonToDelete = (JSONObject) parser.parse(resultStr);
                        /** String dateStr = jsonToDelete.get("DATE").toString();
                         * SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                         * Date parsedDate;
                         * parsedDate = new java.sql.Date(dateFormat.parse(dateStr.replaceAll("\"", "")).getTime());
                         * jsonToDelete.remove("DATE");
                         * jsonToDelete.put("DATE", parsedDate.getTime());
                         **/
                        String dateStr = jsonToDelete.get("DATE").toString();
                        jsonToDelete.remove("DATE");
                        jsonToDelete.put("DATE", dateStr.replaceAll("\"",""));
                        jsonToDelete.put("TYPE", "DELETE");
                        jsonToDelete.put("JSON", "BELARUSBANK");
                        try {
                            ClientsNetworking.sendDataAndGetResponce(jsonToDelete.toJSONString(), gui);
                        } catch (IOException ex) {
                             gui.getConsole().append(ex.getLocalizedMessage());
                        }
                    } else gui.getConsole().append("По вашему запросу обнаружено несколко клиентов. Укажите больше данных: \n" + getJSONResults(json, "FIND",gui));
                    break;
                }
            default:
                break;
        }
return null;
    } 
    private static void insertJSONIntoGUI(JSONObject json, UI gui){
        gui.getWrite().setEnabled(false);
        gui.getDelete().setEnabled(false);
        gui.getFind().setEnabled(false);
        gui.getDisabilities().setEnabled(true);
        gui.getCitizenship().setEnabled(true);
        gui.getMaritalStatus().setEnabled(true);
        gui.getMilitaryService().setEnabled(true);
        gui.getPension().setEnabled(true);
        gui.getGenderF().setEnabled(true);
        gui.getGenderM().setEnabled(true);
        gui.getLastName().setText(json.get("LASTNAME").toString());
        gui.getFirstName().setText(json.get("FIRSTNAME").toString());
        gui.getPatronym().setText(json.get("PATRONYM").toString());
        String date = json.get("DATE").toString().replaceAll("\"", "").replaceAll("-", ".");
        String[] dateArr = date.split("\\.");
        date = dateArr[2]+"."+dateArr[1]+"."+dateArr[0];
        gui.getDateOfBirth().setText(date);
        if(Boolean.getBoolean(json.get("GENDER").toString())){
            gui.getGenderM().setSelected(true);
        } else gui.getGenderF().setSelected(true);
        gui.getPassportSer().setText(json.get("PASSPORTSER").toString());
        gui.getPassportNum().setText(json.get("PASSPORTNUM").toString());
        gui.getPlaceOfIssuing().setText(json.get("PLACEOFPASSPORTISSUING").toString());
        gui.getIdentificNumber().setText(json.get("IDENTIFICNUMBER").toString());
        gui.getPlaceOfBirth().setText(json.get("PLACEOFBIRTH").toString());
        gui.getResidenceCity().setText(json.get("RESIDENCECITY").toString());
        gui.getResidenceAddress().setText(json.get("RESIDENCEADDRESS").toString());
        gui.getPhoneHome().setText(json.get("PHONEHOME").toString());
        gui.getPhoneMobile().setText(json.get("PHONEMOBILE").toString());
        gui.getEmail().setText(json.get("EMAIL").toString());
        gui.getWorkPlace().setText(json.get("WORKPLACE").toString());
        gui.getWorkPosition().setText(json.get("WORKPOSITION").toString());
        gui.getRegistrationCity().setText(json.get("REGISTRATIONCITY").toString());
        gui.getRegistrationAddress().setText(json.get("REGISTRATIONADDRESS").toString());
        gui.getMaritalStatus().setSelectedItem(json.get("MARITALSTATUS").toString());
        gui.getCitizenship().setSelectedItem(json.get("CITIZENSHIP").toString());
        gui.getDisabilities().setSelectedItem(json.get("DISABILITIES").toString());
        System.out.println(json.get("PENSION").toString());
        if(Boolean.getBoolean(json.get("PENSION").toString())){
            gui.getPension().setSelected(true);
        }else  gui.getPension().setSelected(false);
        gui.getMonthlyEarnings().setText(json.get("MONTLYEARNINGS").toString());
        if(Boolean.getBoolean(json.get("MILITARYSERVICE").toString())){
            gui.getMilitaryService().setSelected(true);
        }else  gui.getMilitaryService().setSelected(false);
        json.put("TYPE", "DELETE");
        json.put("JSON", "BELARUSBANK");
        if(Boolean.getBoolean(json.get("GENDER").toString())){
        json.replace("GENDER", 1);
        } else json.replace("GENDER", 0);
        if(Boolean.getBoolean(json.get("PENSION").toString())){
        json.replace("PENSION", 1);
        } else json.replace("PENSION", 0);
        if(Boolean.getBoolean(json.get("MILITARYSERVICE").toString())){
        json.replace("MILITARYSERVICE", 1);
        } else json.replace("MILITARYSERVICE", 0);
        try {
            ClientsNetworking.sendDataAndGetResponce(json.toJSONString(), gui);
        } catch (IOException | java.text.ParseException ex) {
             gui.getConsole().append(ex.getLocalizedMessage());
        }
    }
}