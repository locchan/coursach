/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ClientsModule;

import java.io.IOException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

/**
 *
 * @author Administrator
 */
public class Client {
        public static ClientsModule.UI gui;
     public static int getDataAndSendJSON(String jsontype) throws IOException{
        try {
            
            if((jsontype.equals("INSERT"))&&(gui.getFirstName().getText().trim().equals("")||gui.getLastName().getText().trim().equals("")||
                    gui.getPatronym().getText().trim().equals("")||gui.getPlaceOfIssuing().getText().trim().equals("")||
                    gui.getFirstName().getText().trim().equals("")||gui.getRegistrationAddress().getText().trim().equals(""))){
                    gui.getConsole().append("Не введено обязательное поле\n");
                return 0;
            }
            
            
            
            SimpleDateFormat sdf = new SimpleDateFormat("dd.mm.YYYY");
            boolean gender = false;
            if(gui.getGenderM().isEnabled()||gui.getGenderF().isEnabled()){
            gender = gui.getGenderM().isSelected();
            } else {
                if(jsontype.equals("INSERT")){
                gui.getAdditionConsole().append("Не указан пол!"+"\n");
                return 0;
                }
            }
            String passportser = "";
            if(gui.getPassportSer().getText().length()!=2&&jsontype.equals("INSERT")){
                gui.getAdditionConsole().append("Неверная серия паспорта!"+"\n");
                return 0;
            }else{
                if(passportser.equals("")){
                passportser = gui.getPassportSer().getText();
                }
            }
            String emailAddr = "";
            if(!gui.getEmail().getText().equals("")){
            if(gui.getEmail().getText().contains("@")&&gui.getEmail().getText().contains(".")){
            emailAddr = gui.getEmail().getText();
            } else {
                if(jsontype.equals("INSERT")&&emailAddr.equals("")){
                gui.getAdditionConsole().append("Неверный E-mail адрес!"+"\n");
                return 0;
                }
            }
            }
            
            int passportnum = 0;
            if(!gui.getPassportNum().getText().trim().equals("")){
                passportnum = Integer.parseInt(gui.getPassportNum().getText());
            }
            Date date;
            if(!gui.getDateOfBirth().getText().trim().equals("")){
            date = new Date(sdf.parse(gui.getDateOfBirth().getText()).getTime()+345600000L);
            String[] dateArr = gui.getDateOfBirth().getText().split("\\.");
            int day = Integer.parseInt(dateArr[0]);
            int month = Integer.parseInt(dateArr[1]);
            int year = Integer.parseInt(dateArr[2]);
            if(day>31 || month>12 || (month==2&&day>28)){
            gui.getAdditionConsole().append("Неверная дата рождения\n");
            return 0;
            }
            if(Long.compare(date.getTime(),-2208988800000L)<=0 || (new Date(System.currentTimeMillis()).getYear()-18)<date.getYear()){
                gui.getAdditionConsole().append("Неверная дата рождения\n");
                return 0;
            }
            } else date = new Date(0L);
            int monthlyEarnings = 0;
            try{
            if(!gui.getMonthlyEarnings().getText().trim().equals("")){
            monthlyEarnings = Integer.parseInt(gui.getMonthlyEarnings().getText());
            if(monthlyEarnings<0){
                gui.getAdditionConsole().append("Неверный ежемесячный доход\n");
                return 0;
            }
            }
            }catch(NumberFormatException e){
                //e.printStackTrace();
                gui.getAdditionConsole().append("Неверный ежемесячный доход\n");
                return 0;
            }
            String maritalStatus = "";
            if(gui.getMaritalStatus().isEnabled()){
                maritalStatus = gui.getMaritalStatus().getSelectedItem().toString();
            }
            String citizenship = "";
            if(gui.getCitizenship().isEnabled()){
                citizenship = gui.getCitizenship().getSelectedItem().toString();
            }
            String disabilities = "";
            if(gui.getDisabilities().isEnabled()){
                disabilities = gui.getDisabilities().getSelectedItem().toString();
            }
            String pension = "";
            if(gui.getPension().isEnabled()){
                pension = String.valueOf(gui.getPension().isSelected());
            }
            String militaryService = "";
            if(gui.getMilitaryService().isEnabled()){
                militaryService = String.valueOf(gui.getMilitaryService().isSelected());
            }
            String phoneHome = "";
            String phoneMobile = "";
            try{
            if(!gui.getPhoneHome().getText().equals("")){
            String[] phonHomArr = gui.getPhoneHome().getText().split("\\+");
            Long.parseLong(phonHomArr[1]);
            }
            if(!gui.getPhoneMobile().getText().equals("")){
            String[] phonMobArr = gui.getPhoneMobile().getText().split("\\+");
            Long.parseLong(phonMobArr[1]);
            }
            } catch(NumberFormatException | ArrayIndexOutOfBoundsException e){
                //e.printStackTrace();
                gui.getAdditionConsole().append("Неверный номер мобильного или домашнего телефона\n"
                        + "Телефоны следует вводить в международном формате\n");
                return 0;
            }
            ClientsNetworking.sendDataAndGetResponce(json.createJSON(jsontype,
                    gui.getLastName().getText(),
                    gui.getFirstName().getText(),
                    gui.getPatronym().getText(),
                    date,
                    String.valueOf(genderToBit(gender, gui)),
                    passportser,
                    passportnum,
                    gui.getPlaceOfIssuing().getText(),
                    gui.getIdentificNumber().getText(),
                    gui.getPlaceOfBirth().getText(),
                    gui.getResidenceCity().getText(),
                    gui.getResidenceAddress().getText(),
                    gui.getPhoneHome().getText(),
                    gui.getPhoneMobile().getText(),
                    emailAddr,
                    gui.getWorkPlace().getText(),
                    gui.getWorkPosition().getText(),
                    gui.getRegistrationCity().getText(),
                    gui.getRegistrationAddress().getText(),
                    maritalStatus,
                    citizenship,
                    disabilities,
                    pension,
                    Long.toString(monthlyEarnings),
                    militaryService), gui);
        } catch (ParseException ex) {
                ex.printStackTrace();
                gui.getAdditionConsole().append(ex.getLocalizedMessage()+"\n");
                return 0;
            }
        return 1;
    }
     public static String genderToBit(boolean b, UI gui) {
         if(gui.getGenderF().isEnabled()||gui.getGenderM().isEnabled()){
    if (b)
        return "1";
    else
        return "0";
         } else return "";
}
}
