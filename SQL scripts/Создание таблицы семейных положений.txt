CREATE TABLE `marital_status` (
  `idmaritage_status` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`idmaritage_status`),
  UNIQUE KEY `status_UNIQUE` (`status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
INSERT INTO `marital_status` (`idmaritage_status`,`status`) VALUES (1,'Замужем(женат)');
INSERT INTO `marital_status` (`idmaritage_status`,`status`) VALUES (2,'Не замужем(не женат)');
INSERT INTO `marital_status` (`idmaritage_status`,`status`) VALUES (3,'Разведён(разведена)');